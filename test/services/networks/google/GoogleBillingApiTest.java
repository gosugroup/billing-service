package services.networks.google;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import play.db.jpa.JPA;
import play.libs.Json;
import play.mvc.Result;
import play.test.FakeRequest;
import services.BillingResponse;
import services.BillingResult;
import services.utils.JPAUtils;

import static org.fest.assertions.Assertions.assertThat;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;



public class GoogleBillingApiTest {

    private static final String purchaseData =
            "{\"orderId\":\"12999763169054705758.1332024401521612\",\"packageName\":\"air.air.com.gosugroup.battlefrontheroes\",\"productId\":\"600\",\"purchaseTime\":1395910480051,\"purchaseState\":0,\"developerPayload\":\"1394771666#1\",\"purchaseToken\":\"plhjjakkabhnkipkjoffpngj.AO-J1OyZH4wWyUO0f1Aa6Jhiroe9VP_AJm-T3iBtCy1945l0EPRzt-_G9P57-n3_gAR5cb8A7gJ0qh-ooEaknEkmWVn4rSgJWdIikmquCu9Z0DGBP_RpCf6nuKJvQftHDg-Up8GXdiT9\"}";
    private static final String dataSignature = "ME1dnQLu5KaLYbXvo+CuwdTjH2we6mY0MIHAMOdLCvFni2jQctx7OY6fSphielZxO4PMo2qCwny498zjtC5qzQNH/zfmViK7bNrePRT+tMaC/n32cIR5IcDcLaBIVFkAr4JoXXZ245mrOEoH99AJ/OEFXbKuCPTeTWATCOjIxF/2C85eJLzDjdOsSg17CA1u+VVlYJ3Wu5tU00IvMHOn9TmjWhKLyUfiTfOqwfKShH0cG/qYBXo4MlP7lnE849ayxQLfv6kOZeC5EaYzwihcwlA+d/RQEDiSOfCnoCApRfWq7Oyn60N+75l0y0106J4SWbYffsYEsuuB3/wBNxlZ/Q==";

    private static class HandlePaymentRequest {
        public String userId;
        public String purchaseData;
        public String dataSignature;
        public boolean markDelivered;

        public HandlePaymentRequest(String userId, String purchaseData, String dataSignature, boolean markDelivered) {
            this.userId = userId;
            this.purchaseData = purchaseData;
            this.dataSignature = dataSignature;
            this.markDelivered = markDelivered;
        }
    }

    private static class MarkDeliveryRequest {
        public String orderId;
        public boolean markDelivered;

        public MarkDeliveryRequest(String orderId, boolean markDelivered) {
            this.orderId = orderId;
            this.markDelivered = markDelivered;
        }
    }

    @Before
    public void setUp() {
        running(fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                JPAUtils.deleteAll(GoogleBillingTransaction.class);
            });
        });
    }

    @Test
    public void testHandlePayment() {

        running(testServer(3333, fakeApplication()), () -> {
            FakeRequest request = fakeRequest(POST, "/billing/0.1/api/billing_dev/gp/handlePayment")
                    .withHeader("api_token", "12345678-1234-1234-1234-123456789abc")
                    .withJsonBody(Json.toJson(
                            new HandlePaymentRequest("1#123", purchaseData, dataSignature, true)));
            Result result = routeAndCall(request, 100);
            assertThat(status(result)).isEqualTo(OK);
            Assert.assertEquals(BillingResult.DELIVERED.getCode(),
                    BillingResponse.fromJson(contentAsString(result)).getCode());


            request = fakeRequest(POST, "/billing/0.1/api/billing_dev/gp/markDelivery")
                    .withHeader("api_token", "12345678-1234-1234-1234-123456789abc")
                    .withJsonBody(Json.toJson(new MarkDeliveryRequest("12999763169054705758.1332024401521612", true)));
            result = routeAndCall(request, 100);
            assertThat(status(result)).isEqualTo(OK);
            Assert.assertEquals(BillingResult.DELIVERED.getCode(),
                    BillingResponse.fromJson(contentAsString(result)).getCode());
        });

    }

}
