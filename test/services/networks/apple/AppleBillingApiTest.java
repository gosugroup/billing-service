package services.networks.apple;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import play.db.jpa.JPA;
import play.libs.Json;
import play.mvc.Result;
import play.test.FakeRequest;
import services.BillingResponse;
import services.BillingResult;
import services.utils.JPAUtils;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.*;


public class AppleBillingApiTest {

    private static final String testReceipt = "{\n\t\"signature\" = \"ApkC6T66dgMZXtXPxoREflJ2bXo/rTmFwPAz2t0XpK3cpib89ZNboTiVdzMMV5ZmW16H4vlv8yoTA+eptL4ppFf3dPBth5UdAg+M+KMFfS8TI/SrOBD9Fo4Nx1iQa4z6SPxh5oUcJMLrW9z85T3hQFqLw5HzcxvQ+84p0BJWZl5+AAADVzCCA1MwggI7oAMCAQICCGUUkU3ZWAS1MA0GCSqGSIb3DQEBBQUAMH8xCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSYwJAYDVQQLDB1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEzMDEGA1UEAwwqQXBwbGUgaVR1bmVzIFN0b3JlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTA5MDYxNTIyMDU1NloXDTE0MDYxNDIyMDU1NlowZDEjMCEGA1UEAwwaUHVyY2hhc2VSZWNlaXB0Q2VydGlmaWNhdGUxGzAZBgNVBAsMEkFwcGxlIGlUdW5lcyBTdG9yZTETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMrRjF2ct4IrSdiTChaI0g8pwv/cmHs8p/RwV/rt/91XKVhNl4XIBimKjQQNfgHsDs6yju++DrKJE7uKsphMddKYfFE5rGXsAdBEjBwRIxexTevx3HLEFGAt1moKx509dhxtiIdDgJv2YaVs49B0uJvNdy6SMqNNLHsDLzDS9oZHAgMBAAGjcjBwMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUNh3o4p2C0gEYtTJrDtdDC5FYQzowDgYDVR0PAQH/BAQDAgeAMB0GA1UdDgQWBBSpg4PyGUjFPhJXCBTMzaN+mV8k9TAQBgoqhkiG92NkBgUBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAEaSbPjtmN4C/IB3QEpK32RxacCDXdVXAeVReS5FaZxc+t88pQP93BiAxvdW/3eTSMGY5FbeAYL3etqP5gm8wrFojX0ikyVRStQ+/AQ0KEjtqB07kLs9QUe8czR8UGfdM1EumV/UgvDd4NwNYxLQMg4WTQfgkQQVy8GXZwVHgbE/UC6Y7053pGXBk51NPM3woxhd3gSRLvXj+loHsStcTEqe9pBDpmG5+sk4tw+GK3GMeEN5/+e1QT9np/Kl1nj+aBw7C0xsy0bFnaAd1cSS6xdory/CUvM6gtKsmnOOdqTesbp0bs8sn6Wqs0C9dgcxRHuOMZ2tm8npLUm7argOSzQ==\";\n\t\"purchase-info\" = \"ewoJIm9yaWdpbmFsLXB1cmNoYXNlLWRhdGUtcHN0IiA9ICIyMDE0LTAzLTMxIDAyOjA4OjU0IEFtZXJpY2EvTG9zX0FuZ2VsZXMiOwoJInVuaXF1ZS1pZGVudGlmaWVyIiA9ICI2NWI3MmQ1ZWVmNDcyYjA2NzY3ODJhZjdlMjFiMDhhMTkwYjNiODZlIjsKCSJvcmlnaW5hbC10cmFuc2FjdGlvbi1pZCIgPSAiMTAwMDAwMDEwNjI0OTU0OSI7CgkiYnZycyIgPSAiMC4xLjEiOwoJInRyYW5zYWN0aW9uLWlkIiA9ICIxMDAwMDAwMTA2MjQ5NTQ5IjsKCSJxdWFudGl0eSIgPSAiMSI7Cgkib3JpZ2luYWwtcHVyY2hhc2UtZGF0ZS1tcyIgPSAiMTM5NjI1NjkzNDAzMCI7CgkidW5pcXVlLXZlbmRvci1pZGVudGlmaWVyIiA9ICI2MEI1MjA0MC1GNEQ5LTQ1NzYtOEIzRS0wMjkxQjk3NzA2NzYiOwoJInByb2R1Y3QtaWQiID0gIjYwMiI7CgkiaXRlbS1pZCIgPSAiODQ3MzM4Mjk5IjsKCSJiaWQiID0gImFpci5jb20uZ29zdWdyb3VwLmJhdHRsZWZyb250aGVyb2VzIjsKCSJwdXJjaGFzZS1kYXRlLW1zIiA9ICIxMzk2MjU2OTM0MDMwIjsKCSJwdXJjaGFzZS1kYXRlIiA9ICIyMDE0LTAzLTMxIDA5OjA4OjU0IEV0Yy9HTVQiOwoJInB1cmNoYXNlLWRhdGUtcHN0IiA9ICIyMDE0LTAzLTMxIDAyOjA4OjU0IEFtZXJpY2EvTG9zX0FuZ2VsZXMiOwoJIm9yaWdpbmFsLXB1cmNoYXNlLWRhdGUiID0gIjIwMTQtMDMtMzEgMDk6MDg6NTQgRXRjL0dNVCI7Cn0=\";\n\t\"environment\" = \"Sandbox\";\n\t\"pod\" = \"100\";\n\t\"signing-status\" = \"0\";\n}";

    private static class JsonPara {
        public String userId;
        public String transactionId;
        public String clientReceipt;
        public boolean markDelivered;


        public JsonPara(String userId, String transactionId, String clientReceipt, boolean markDelivered) {
            this.userId = userId;
            this.transactionId = transactionId;
            this.clientReceipt = clientReceipt;
            this.markDelivered = markDelivered;
        }
    }


    @Before
    public void setUp() {
        running(fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                JPAUtils.deleteAll(AppleBillingTransaction.class);
            });
        });
    }

    @Test
    public void testInitPurchase() {

        running(testServer(3333, fakeApplication()), () -> {
            FakeRequest request = fakeRequest(POST, "/billing/0.1/api/billing_dev/ios/handlePayment")
                    .withHeader("api_token", "12345678-1234-1234-1234-123456789abc")
                    .withJsonBody(Json.toJson(
                            new JsonPara("1#123", "tx_1", testReceipt, true)));
            Result result = routeAndCall(request, 100);
            assertThat(status(result)).isEqualTo(OK);

            String body = contentAsString(result);
            Assert.assertEquals(BillingResult.DELIVERED.getCode(),
                    BillingResponse.fromJson(body).getCode());
        });

    }


}
