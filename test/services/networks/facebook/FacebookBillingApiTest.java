package services.networks.facebook;

import com.amazonaws.regions.Regions;
import org.junit.Assert;
import models.Game;
import org.junit.Before;
import org.junit.Test;

import play.db.jpa.JPA;
import services.BillingResult;
import services.exception.BillingException;
import services.networks.facebook.graph.FacebookCall;
import services.networks.facebook.graph.payment.DisputeReason;
import services.networks.facebook.graph.payment.DisputeStatus;
import services.networks.facebook.graph.payment.Payment;
import services.utils.JPAUtils;

import java.io.IOException;

import static play.test.Helpers.*;


public class FacebookBillingApiTest {

    // This sample request contains charge, chargeback, chargeback_reversal, and a user dispute.
    // After it's all processed, the user balance shouldn't change.
    String paymentString = "{\n" +
            "    \"id\": \"496810703782460\",\n" +
            "    \"user\": {\n" +
            "        \"id\": \"9527\",\n" +
            "        \"name\": \"Shuping Zhou\"\n" +
            "    },\n" +
            "    \"application\": {\n" +
            "        \"name\": \"[QA] Battlefront Heroes\",\n" +
            "        \"namespace\": \"qa-gosu-bh\",\n" +
            "        \"id\": \"1396284503941935\"\n" +
            "    },\n" +
            "    \"actions\": [{\n" +
            "        \"type\": \"charge\",\n" +
            "        \"status\": \"completed\",\n" +
            "        \"currency\": \"HKD\",\n" +
            "        \"amount\": \"40.00\",\n" +
            "        \"time_created\": \"2014-06-17T11:45:50+0000\",\n" +
            "        \"time_updated\": \"2014-06-17T11:45:52+0000\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"type\": \"chargeback\",\n" +
            "        \"status\": \"completed\",\n" +
            "        \"currency\": \"HKD\",\n" +
            "        \"amount\": \"40.00\",\n" +
            "        \"time_created\": \"2014-06-17T11:45:50+0000\",\n" +
            "        \"time_updated\": \"2014-06-17T11:45:52+0000\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"type\": \"chargeback_reversal\",\n" +
            "        \"status\": \"completed\",\n" +
            "        \"currency\": \"HKD\",\n" +
            "        \"amount\": \"40.00\",\n" +
            "        \"time_created\": \"2014-06-17T11:45:50+0000\",\n" +
            "        \"time_updated\": \"2014-06-17T11:45:52+0000\"\n" +
            "    }],\n" +
            "    \"refundable_amount\": {\n" +
            "        \"currency\": \"HKD\",\n" +
            "        \"amount\": \"40.00\"\n" +
            "    },\n" +
            "    \"items\": [{\n" +
            "        \"type\": \"IN_APP_PURCHASE\",\n" +
            "        \"product\": \"https:\\/\\/s3.amazonaws.com\\/gosu_bill\\/x\\/static\\/products\\/600\",\n" +
            "        \"quantity\": 1\n" +
            "    }],\n" +
            "    \"country\": \"US\",\n" +
            "    \"created_time\": \"2014-06-17T11:45:50+0000\",\n" +
            "    \"test\": 1,\n" +
            "    \"payout_foreign_exchange_rate\": 0.12770901,\n" +

            "    \"disputes\": [\n" +
            "    {\n" +
            "      \"user_comment\": \"I didn't get my Friend Smash coin! Please help!\", \n" +
            "      \"time_created\": \"2014-02-12T01:37:27+0000\", \n" +
            "      \"user_email\": \"mail@domain.com\", \n" +
            "      \"status\": \"pending\", \n" +
            "      \"reason\": \"pending\"\n" +
            "    }\n" +
            "  ]" +
            "}";


    @Before
    public void setUp() {
        running(fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                JPAUtils.deleteAll(FacebookBillingTransaction.class);
            });
        });
    }

    @Test
    public void testProcessPaymentUpdate() {

        running(testServer(3333, fakeApplication()), new Runnable() {
            @Override
            public void run() {

                Game ce = new Game();
                ce.setId("billing_dev");
                ce.setAwsRegionName(Regions.SA_EAST_1);
                ce.setAwsAccessKey("AKIAJS4MQKXLER3656CA");
                ce.setAwsSecretKey("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");
                ce.setAwsSqsUrl("https://sqs.ap-southeast-1.amazonaws.com/103304292377/gosu_billing_shuping_dev");

                FacebookCall facebookCall = new FacebookCall("fake", "fake") {

                    @Override
                    public Payment getFacebookPayment(String transactionId) throws IOException {
                        return FacebookCall.paymentReader.readValue(paymentString);
                    }

                    @Override
                    public void refundFacebookPayment(String paymentId, String currency, double amount) throws IOException, FacebookGraphApiException {

                    }

                    @Override
                    public void resolveFacebookDispute(String paymentId, DisputeReason reason) throws IOException, FacebookGraphApiException {

                    }
                };

                FacebookBillingClient client = new FacebookBillingClient(ce, facebookCall);

                Assert.assertEquals(BillingResult.OK, client.processPaymentUpdate("496810703782460"));
                Assert.assertEquals(BillingResult.OK, client.processPaymentUpdate("496810703782460"));
                Assert.assertEquals(BillingResult.RESOLVED.getCode(), client.resolvePaymentDispute("496810703782460", "HKD", 20.0, null, null).getCode());

            }
        });

    }

    @Test
    public void testDecodeSignedRequest() throws BillingException {
        String s = "Lk2ECapqmEMnSodR65ILZqhJqlGGT9MezFXupdLl-1g.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImFtb3VudCI6IjQ5LjkwIiwiY3VycmVuY3kiOiJVU0QiLCJpc3N1ZWRfYXQiOjE0Mjg2Mzc3NTQsInBheW1lbnRfaWQiOjYyNzczODgwNDAyMzI4MiwicXVhbnRpdHkiOiIxMCIsInJlcXVlc3RfaWQiOiIxMDIwNjQ1OTY5OTc1MDA2NV82MDBfMTQyODYzNzc0NDMzNiIsInN0YXR1cyI6ImNvbXBsZXRlZCJ9";
        FacebookBillingClient client = new FacebookBillingClient(new Game());
        client.decodeSignedRequest(s, "3d29301c421682b625b48e8b63031b96");
    }


    @Test
    public void testFacebookTransactionDao() {

        running(fakeApplication(), () -> {
            FacebookBillingTransaction tx = new FacebookBillingTransaction();
            tx.setOrderId("1");
            tx.setGameId("billing_dev");
            tx.setUserId("123");
            tx.setCurrency("USD");
            tx.setProductId("123");
            tx.setProductQuantity(1);
            tx.setStatus(FacebookBillingTransactionStatus.CHARGE);

            FacebookUserDispute dispute = new FacebookUserDispute();
            dispute.setOrderId("1");
            dispute.setRefundedAmount(10);
            dispute.setReason(DisputeReason.PENDING);
            dispute.setStatus(DisputeStatus.PENDING);

            tx.getDisputes().add(dispute);

            JPA.withTransaction(() -> {
                JPA.em().persist(tx);
            });

            JPA.withTransaction(() -> {
                FacebookBillingTransaction tx2 = JPA.em().find(FacebookBillingTransaction.class, "1");
                Assert.assertEquals(10, tx2.getDisputes().get(0).getRefundedAmount(), 0.001);
            });
        });
    }


}
