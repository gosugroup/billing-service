import java.lang.reflect.Method;

import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.Logger.ALogger;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Result;
import services.BillingResult;
import services.exception.BillingApiException;

import static play.mvc.Results.internalServerError;
import static play.mvc.Results.ok;

public class Global extends GlobalSettings {

    private final ALogger LOG = Logger.of("access");

    @Override
    @SuppressWarnings("rawtypes")
    public Action onRequest(Request request, Method method) {
        LOG.info(request.method() + ": " + request.uri() + ", from: " + request.remoteAddress());

        return super.onRequest(request, method);
    }

    @Override
    public F.Promise<Result> onError(Http.RequestHeader request, Throwable t) {
        LOG.error("OnError: {}", t.getMessage());

        Throwable cause = t.getCause();
        if (cause != null && cause instanceof BillingApiException) {
            return F.Promise.pure(
                    ok(BillingResult.FAILED.toBillingResponse(cause.getMessage()).toJson()));
        }
        return null;
    }

    @Override
    public void onStart(Application app) {
        LOG.info("Application has started");

//        Akka.system().scheduler().schedule(
//                Duration.create(10, TimeUnit.SECONDS),
//                Duration.create(60, TimeUnit.SECONDS),
//                () -> {
//                    LOG.info("Akka scheduled job...");
//                    Game ce = new GameDao().findWithCache("ce");
//                    SQSSyncClient sqs = new SQSSyncClient(ce.getAwsRegionName(), ce.getAwsAccessKey(), ce.getAwsSecretKey(), ce.getAwsSqsUrl());
//                    List<Message> messages = sqs.getMessages();
//                    if (messages.isEmpty()) {
//                        return;
//                    }
//
//                    for (Message message : messages) {
//                        LOG.debug("Received message: {}", message);
//                    }
//
//                    DeleteMessageBatchResult result = sqs.aknowledgeMessages(messages);
//                    LOG.debug("ack result: {}", result);
//
//                },
//                Akka.system().dispatcher()
//        );
    }

    @Override
    public void onStop(Application app) {
        LOG.info("Application has stopped");
    }

}