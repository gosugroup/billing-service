package services;

import models.Game;
import play.Logger;
import play.db.jpa.JPA;
import services.exception.BillingException;
import services.products.Product;
import services.stats.BillingEventType;
import services.stats.UpsightClient;

import java.util.Date;

/**
 * Created by shuping on 4/16/15.
 */
public class BillingClient<T extends BillingTransaction> {

    private static final Logger.ALogger LOG = Logger.of(BillingClient.class);

    protected ClientType clientType;
    protected Game game;
    protected UpsightClient upsight;

    private Class<T> transactionClass;

    public BillingClient(ClientType clientType, Game game, UpsightClient upsight, Class<T> transactionClass) {
        this.clientType = clientType;
        this.game = game;
        this.upsight = upsight;
        this.transactionClass = transactionClass;
    }

    protected BillingResponse handlePayment(T tx, boolean markDelivered, boolean sendPurchaseEvent) {

        try{
            final Product product = game.getProductFactory().getProduct(tx.getProductId());
            if (product == null) {
                throw new BillingException("Configuration not found for billing productId: " + tx.getProductId());
            }

            return JPA.withTransaction(() -> {
                final T loadedTx = JPA.em().find( (Class<T>) tx.getClass(), tx.getOrderId());
                if (loadedTx != null) {
                    LOG.error("Transaction already exists: {}", tx.getOrderId());
                    return BillingResult.ALREADY_EXISTS.toBillingResponse();
                }

                BillingResult result = setTransactionDelivery(tx, markDelivered);
                JPA.em().persist(tx);

                if (sendPurchaseEvent) {
                    final float price = (float) (product.getPrice("USD") * tx.getProductQuantity());
                    upsight.sendBillingEvent(clientType, tx.getUserId(), price, tx.getProductId(),
                            BillingEventType.PURCHASE, null);
                }

                return result.toBillingResponse();
            });

        } catch (Throwable t) {
            LOG.error("Failed to initiate transaction: ", t);
            return BillingResult.FAILED.toBillingResponse(t);
        }
    }

    public BillingResponse markDelivery(String transactionId, boolean delivered) {
        try {
            return JPA.withTransaction(() -> {
                final T tx = JPA.em().find(transactionClass, transactionId);
                if (tx == null) {
                    LOG.error("Transaction doesn't exist: {}", transactionId);
                    return BillingResult.NOT_EXISTS.toBillingResponse();
                }

                BillingResult result = setTransactionDelivery(tx, delivered);
                return result.toBillingResponse();
            });
        } catch (Throwable t) {
            LOG.error("Failed to mark transaction delivered:", t);
            return BillingResult.FAILED.toBillingResponse(t);
        }
    }

    private BillingResult setTransactionDelivery(BillingTransaction tx, boolean delivered) {
        BillingResult result;
        if (delivered) {
            tx.setDeliveryFinished(new Date());
            tx.setDeliveryPending(false);
            result = BillingResult.DELIVERED;
        } else {
            tx.setDeliveryFinished(null);
            tx.setDeliveryPending(true);
            result = BillingResult.PURCHASED;
        }
        return result;
    }

}
