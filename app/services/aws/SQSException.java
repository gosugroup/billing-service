package services.aws;

/**
 * Created by shuping on 4/8/15.
 */
public class SQSException extends Exception {

    public SQSException() {
        super();
    }

    public SQSException(String message) {
        super(message);
    }

    public SQSException(String message, Throwable cause) {
        super(message, cause);
    }

    public SQSException(Throwable cause) {
        super(cause);
    }
}
