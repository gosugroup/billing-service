package services.aws;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonWebServiceRequest;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.handlers.AsyncHandler;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceAsyncClient;
import com.amazonaws.services.simpleemail.model.*;
import play.Logger;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by shuping on 4/13/15.
 */
public class SESAsyncClient {

    private static final Logger.ALogger LOG = Logger.of(SESAsyncClient.class);

    private final AmazonSimpleEmailServiceAsyncClient client;

    static final AsyncHandler<SendEmailRequest,SendEmailResult> asyncHandler = new AsyncHandler(){
        @Override
        public void onError(Exception e) {
            LOG.error("Failed to send email: {}", e);
        }
        @Override
        public void onSuccess(AmazonWebServiceRequest request, Object result) {
            SendEmailResult sendResult = (SendEmailResult) result;
            LOG.info("Email sent: {}", sendResult.getMessageId());
        }
    };

    public SESAsyncClient(Regions regionName, String accessKey, String secretKey) {
        client = new AmazonSimpleEmailServiceAsyncClient(
                new BasicAWSCredentials(accessKey, secretKey),
                Executors.newSingleThreadExecutor()); // Amazon Maximum sending rate: 5 messages/second.
        client.setRegion(Region.getRegion(regionName));
    }

    public boolean sendHtmlEmail(String from, String[] tos, String[] ccs, String subject, String body) {
        return sendEmail(from, tos, ccs, new String[]{}, new String[]{},
                subject, body, "UTF8", true, true);
    }

    public boolean sendTextEmail(String from, String[] tos, String[] ccs, String subject, String body) {
        return sendEmail(from, tos, ccs, new String[]{}, new String[]{},
                subject, body, "UTF8", false, true);
    }

    public boolean sendEmail(String from, String[] tos, String[] ccs, String[] bccs, String[] replyTos,
                             String subject, String body, String charset, boolean html, boolean blocking) {
        // Construct an object to contain the recipient address.
        Destination destination = new Destination().withToAddresses(tos).withCcAddresses(ccs).withBccAddresses(bccs);

        // Create the subject and body of the message.
        Content _subject = new Content().withCharset(charset).withData(subject);
        Content bodyContent = new Content().withCharset(charset).withData(body);
        Body _body = html ? new Body().withHtml(bodyContent) : new Body().withText(bodyContent);

        // Create a message with the specified subject and body.
        Message message = new Message().withSubject(_subject).withBody(_body);

        // Assemble the email.
        SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message).withReplyToAddresses(replyTos);

        try
        {
            LOG.debug("Sending email: {}...", subject);
            Future<SendEmailResult> result = client.sendEmailAsync(request, asyncHandler);
            return blocking ? (result.get() != null) : true;
        }
        catch (AmazonClientException | InterruptedException | ExecutionException ex)
        {
            LOG.error("Failed to send email: " + ex.getMessage());
            return false;
        }
    }

    public void shutdown() {
        try {
            client.getExecutorService().shutdown();
            client.getExecutorService().awaitTermination(30, TimeUnit.SECONDS);
            client.shutdown();
        } catch (InterruptedException ex) {
            LOG.error("Interrupted waiting for email executor service shutting down.", ex);
        }
    }

}
