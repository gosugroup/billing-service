package services.aws;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SQSSyncClient {

    private final static int MAX_MESSAGES = 10;

    public final static String ATTR_NUM_VISIBLE_MESSAGES = "ApproximateNumberOfMessages";
    public final static String ATTR_NUM_INVISIBLE_MESSAGES = "ApproximateNumberOfMessagesNotVisible";


    private final AmazonSQSClient client;
    private final String queueUrl;

    public SQSSyncClient(Regions regionName, String accessKey, String secretKey, String queueUrl) {
        this.client = buildClient(regionName, accessKey, secretKey);
        this.queueUrl = queueUrl;
    }

    private AmazonSQSClient buildClient(Regions regionName, String accessKey, String secretKey) {
        final AmazonSQSClient client = new AmazonSQSClient(
                new BasicAWSCredentials(accessKey, secretKey));
        client.setRegion(regionName);
        return client;
    }

    public SendMessageResult sendMessage(String body) throws SQSException {
        try {
            final SendMessageResult result = client.sendMessage(
                    new SendMessageRequest(queueUrl, body)
            );
            return result;

        } catch (Exception e) {
            throw new SQSException(e);
        }
    }

    public List<Message> getMessages() {
        final ReceiveMessageResult result = client.receiveMessage(
                new ReceiveMessageRequest(queueUrl)
                        .withMaxNumberOfMessages(MAX_MESSAGES)
                        .withVisibilityTimeout(30)
                        .withWaitTimeSeconds(20)
        );
        return result.getMessages();
    }

    public DeleteMessageBatchResult deleteMessages(DeleteMessageBatchRequest request) {
        return client.deleteMessageBatch(request.withQueueUrl(queueUrl));
    }

    public DeleteMessageBatchResult aknowledgeMessages(List<Message> msgs) {
        List<DeleteMessageBatchRequestEntry> toDelete = new ArrayList<>();
        for (Message msg: msgs) {
            toDelete.add(new DeleteMessageBatchRequestEntry(msg.getMessageId(), msg.getReceiptHandle()));
        }
        return client.deleteMessageBatch(new DeleteMessageBatchRequest(queueUrl, toDelete));
    }

    public Map<String, String> readQueueStats() {
        GetQueueAttributesResult result = client.getQueueAttributes(new GetQueueAttributesRequest()
                .withAttributeNames("All")
                .withQueueUrl(queueUrl));

        return result.getAttributes();
    }
}
