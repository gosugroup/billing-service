package services;

public enum BillingResult {

    OK(0),                      // a generic code for success

    PURCHASED(1),               // recorded the transaction
    DELIVERED(2),               // marked transaction as delivered in billing db
    RESOLVED(3),                // the transaction resolved with other actions (like dispute)

    ALREADY_EXISTS(1001),       // the transaction already exists
    NOT_EXISTS(1002),           // the transaction doesn't recorded before
    INVALID_TRANSACTION(1003),  // the transaction is not valid (wrong signature or etc.)

    FAILED(2000),               // other failures
    INTERNAL_ERROR(2001),       // billing service internal errors
    ;

    private final int code;

    private BillingResult(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public BillingResponse toBillingResponse() {
        return new BillingResponse(this);
    }

    public BillingResponse toBillingResponse(String message) {
        return new BillingResponse(this, message);
    }

    public BillingResponse toBillingResponse(Throwable t) {
        return new BillingResponse(this, t.getMessage());
    }

}
