package services.stats;

/**
* Created by shuping on 11/19/14.
*/
public enum BillingEventType {
    PURCHASE("Purchase", true),
    REFUND("REFUND", false),
    DECLINE("DECLINE", false),
    DISPUTE("DISPUTE", false),
    CHARGEBACK("CHARGEBACK", false),
    CHARGEBACK_REV("CHARGEBACK_REV", true),;

    private String value;
    private boolean isIncome;

    BillingEventType(String value, boolean isIncome) {
        this.value = value;
        this.isIncome = isIncome;
    }

    String getValue() {
        return this.value;
    }

    boolean isIncome() {
        return this.isIncome;
    }

}
