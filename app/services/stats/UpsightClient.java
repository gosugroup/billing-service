package services.stats;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import play.Logger;
import play.libs.ws.WS;
import play.libs.ws.WSRequestHolder;
import services.ClientType;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Created by shuping on 11/18/14.
 */
public class UpsightClient {

    private static final Logger.ALogger LOG = Logger.of(UpsightClient.class);

    public static String PROD_SERVER_HOST = "http://api.geo.kontagent.net";
    public static String TEST_SERVER_HOST = "http://test-server.kontagent.com";

    public static enum TransactionType{
        direct, indirect, advertisement, credits, other
    }

    public static enum CommunicationType{
        ad, partner
    }

    public boolean useTestServer;
    public String apiKey;


    public UpsightClient(boolean useTestServer, String apiKey) {
        this.useTestServer = useTestServer;
        this.apiKey = apiKey;
    }

    private void postEvent(WSRequestHolder request) {
        LOG.debug("Post event to Upsight: {}", request.getUrl());
        request.get().onFailure(e -> {
            LOG.error("Failed to post event to Upsight: {}: {}", request.getUrl(), e);
        });
    }

    public String sendLandingPageEvent(ClientType clientType, HttpServletRequest request) {
        String source = request.getParameter("fb_source");
        String notificationType = request.getParameter("notif_t");

        String shortTrackingTag = "";
        String sUid = null; // cannot get user id on landing page (before user authenticate).
        WSRequestHolder upsightRequest = null;

        try {
            // facebook app invites: upsight invite events.
            if ("notification".equals(source) && "app_invite".equals(notificationType)) {
                String requestIds = request.getParameter("request_ids");
                if (requestIds != null) {
                    String requestId = requestIds.split(",")[0];
                    String trackingTag = hashTrackingTagDecimal("1#" + requestId); //hardcode for fb.
                    upsightRequest = buildInviteReceivedRequest(clientType, false, trackingTag, sUid, null, null, null, null);
                }
            } else {
                // classify all other cases into UCC channel for now.
                shortTrackingTag = genShortUniqueTrackingTag();
                String ktType = request.getParameter("kt_type");
                if ("ad".equals(ktType)) {
                    // upsight ads link example: https://apps.facebook.com/combatelite/?kt_type=ad&kt_st1=test&kt_st2=fb&kt_st3=emailtarget
                    String st1 = request.getParameter("kt_st1");
                    String st2 = request.getParameter("kt_st2");
                    String st3 = request.getParameter("kt_st3");
                    upsightRequest = buildCommunicationClicksRequest(clientType, false, CommunicationType.ad, shortTrackingTag, sUid,
                            st1, st2, st3, null);

                } else {
                    // other facebook sources: partner channels
                    upsightRequest = buildCommunicationClicksRequest(clientType, false, CommunicationType.partner, shortTrackingTag, sUid,
                            "fb_source", source, null, null);
                }
            }

            postEvent(upsightRequest);
            return shortTrackingTag;

        } catch (URISyntaxException | UnsupportedEncodingException e) {
            LOG.error("Failed to build Upsight request", e);
            return null;
        }
    }

    public void sendInstallEvent(ClientType clientType, String uid, String trackingTag, String shortTrackingTag) {
        try {
            WSRequestHolder request = buildApplicationAddedRequest(clientType, hashUserIdDecimal(uid), trackingTag, shortTrackingTag, null);
            postEvent(request);

        } catch (URISyntaxException | UnsupportedEncodingException e) {
            LOG.error("Failed to build Upsight request", e);
        }
    }

    public void sendBillingEvent(ClientType clientType, String uid, float amountUSD, String productId, BillingEventType event, ObjectNode props) {
        try {
            int factor = event.isIncome() ? 1 : -1;
            String jsonData = (props != null ? props.toString() : null);

            WSRequestHolder request = buildBillingRequest(clientType, hashUserIdDecimal(uid), (int) (amountUSD * factor * 100), TransactionType.credits,
                    event.getValue(), productId, null, jsonData);
            postEvent(request);

        } catch (URISyntaxException | UnsupportedEncodingException e) {
            LOG.error("Failed to build Upsight request", e);
        }
    }

    public void sendBillingEvent(ClientType clientType, String uid, float amountUSD, String productId, BillingEventType event, ObjectNode props, Map<String, String> statsMap) {

        statsMap.forEach((k, v) -> props.put(k, v));
        sendBillingEvent(clientType, uid, amountUSD, productId, event, props);
    }

    private WSRequestHolder buildBillingRequest(ClientType clientType, String uid, int cents,
                                    TransactionType type, String subType1, String subType2, String subType3,
                                    String jsonData) throws URISyntaxException, UnsupportedEncodingException {

        WSRequestHolder request = prepareURIBuilder(clientType, "mtu", subType1, subType2, subType3, jsonData)
                .setQueryParameter("s", uid)
                .setQueryParameter("v", String.valueOf(cents));

        if (type != null) {
            request.setQueryParameter("tu", type.name());
        }

        return request;
    }

    private WSRequestHolder buildCommunicationClicksRequest(ClientType clientType, boolean alreadyInstalled, CommunicationType type, String shortTrackingTag, String uid,
                                                String subType1, String subType2, String subType3, String jsonData)
            throws URISyntaxException, UnsupportedEncodingException {

        WSRequestHolder request = prepareURIBuilder(clientType, "ucc", subType1, subType2, subType3, jsonData)
                .setQueryParameter("i", alreadyInstalled ? "1" : "0")
                .setQueryParameter("tu", type.name())
                .setQueryParameter("su", shortTrackingTag);

        if (uid != null) {
            request.setQueryParameter("s", uid);
        }

        return request;
    }

    private WSRequestHolder buildInviteReceivedRequest(ClientType clientType, boolean alreadyInstalled, String uniqueTrackingTag, String uid,
                                           String subType1, String subType2, String subType3, String jsonData)
            throws URISyntaxException, UnsupportedEncodingException {

        WSRequestHolder request = prepareURIBuilder(clientType, "inr", subType1, subType2, subType3, jsonData)
                .setQueryParameter("i", alreadyInstalled ? "1" : "0")
                .setQueryParameter("u", uniqueTrackingTag);

        if (uid != null) {
            request.setQueryParameter("r", uid);
        }

        return request;
    }

    private WSRequestHolder buildApplicationAddedRequest(ClientType clientType, String uid, String uniqueTrackingTag, String shortUniqueTrackingTag, String jsonData) throws UnsupportedEncodingException, URISyntaxException {
        WSRequestHolder request = prepareURIBuilder(clientType, "apa", null, null, null, jsonData)
                .setQueryParameter("s", uid);

        if (uniqueTrackingTag != null) {
            request.setQueryParameter("u", uniqueTrackingTag);
        }

        if (shortUniqueTrackingTag != null) {
            request.setQueryParameter("su", shortUniqueTrackingTag);
        }

        return request;

    }

    private WSRequestHolder prepareURIBuilder(ClientType clientType, String action, String subType1, String subType2, String subType3,
                                              String jsonData) throws UnsupportedEncodingException {
        String apiKey = this.apiKey;
        WSRequestHolder request = WS.url(
                (useTestServer ? TEST_SERVER_HOST : PROD_SERVER_HOST) +
                        "/api/v1/" + apiKey + "/" + action + "/");


        if (subType1 != null) {
            request.setQueryParameter("st1", subType1);
        }

        if (subType2 != null) {
            request.setQueryParameter("st2", subType2);
        }

        if (subType3 != null) {
            request.setQueryParameter("st3", subType3);
        }

        if (jsonData != null) {
            request.setQueryParameter("data", Base64.encodeBase64String(jsonData.getBytes("UTF-8")));
        }

        return request;
    }


    /**
     * Upsight accepts 16 digit hex string as tracking tag.
     * @param input
     * @return
     */
    public static String hashTrackingTagDecimal(String input) {
        String md5 = DigestUtils.md5Hex(input);
        return md5.substring(0, 16);
    }

    /**
     * Upsight accepts 63 bit integer as user id.
     * @param uid
     * @return
     */
    public static String hashUserIdDecimal(String uid) {
        String md5 = DigestUtils.md5Hex(uid);
        md5 = md5.substring(0, 13);
        return Long.valueOf(md5, 16).toString();
    }

    private static String genUniqueTrackingTag() {
        String s = "" + System.currentTimeMillis() + Math.floor(Math.random()*10000);
        String tag = DigestUtils.md5Hex(s);
        return tag.substring(0, 16);
    }

    private static String genShortUniqueTrackingTag() {
        String s = "" + System.currentTimeMillis() + Math.floor(Math.random()*10000);
        String tag = DigestUtils.md5Hex(s);
        return tag.substring(0, 8);
    }


    public static void main(String[] args) throws UnsupportedEncodingException, URISyntaxException, InterruptedException {
        System.out.println(hashUserIdDecimal("1#12345"));

        new UpsightClient(true, "aeac478b26dd4b1d902817563d8f4816").sendBillingEvent(ClientType.WEB, "1#9999", 1.99f, "600", BillingEventType.PURCHASE, null);
        Thread.sleep(10000);
    }

}
