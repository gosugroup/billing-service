package services.stats;


import services.ClientType;

/**
 * Created by shuping on 4/8/15.
 */
public class BillingEvent {

    private BillingEventType eventType;
    private ClientType clientType;
    private String userId;
    private String itemCode;
    private float usdPrice;
    private boolean testMode;

    public BillingEvent(BillingEventType eventType, ClientType clientType, String userId, String itemCode, float usdPrice, boolean testMode) {
        this.eventType = eventType;
        this.clientType = clientType;
        this.userId = userId;
        this.itemCode = itemCode;
        this.usdPrice = usdPrice;
        this.testMode = testMode;
    }

    public BillingEvent(BillingEventType eventType, ClientType clientType, String userId, String itemCode, float usdPrice) {
        this.eventType = eventType;
        this.clientType = clientType;
        this.userId = userId;
        this.itemCode = itemCode;
        this.usdPrice = usdPrice;
        this.testMode = false;
    }


    public BillingEventType getEventType() {
        return eventType;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public String getUserId() {
        return userId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public float getUsdPrice() {
        return usdPrice;
    }

    public boolean isTestMode() {
        return testMode;
    }
}
