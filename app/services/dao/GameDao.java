package services.dao;

import models.Game;
import models.Summary;
import play.Logger;
import play.cache.Cache;
import play.db.jpa.JPA;
import services.aws.SQSSyncClient;
import services.utils.JPAUtils;
import services.utils.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by shuping on 3/27/15.
 */
public class GameDao {

    private static Logger.ALogger LOG = Logger.of(GameDao.class);

    public List<Game> list() {
        try {
            return JPA.withTransaction(() -> JPAUtils.findAll(Game.class));
        } catch (Throwable t) {
            LOG.error("Failed to run transaction: ", t);
            return Collections.EMPTY_LIST;
        }
    }

    public Game find(String id) {
        try {
            return JPA.withTransaction(() -> Game.findById(id));
        } catch (Throwable t) {
            LOG.error("Failed to run transaction: ", t);
            return null;
        }
    }

    public Game findWithCache(String id) {
        try {
            return Cache.getOrElse("game:" + id, () -> find(id), 60);
        } catch (Throwable t) {
            LOG.error("Failed to find game with cache: ", t);
            return null;
        }
    }

    public boolean update(Game game, String id) {
        try {
            JPA.withTransaction(() -> game.update(id));
            return true;
        } catch (Throwable t) {
            LOG.error("Failed to run transaction: ", t);
            return false;
        }
    }

    public boolean save(Game game) {
        try {
            return JPA.withTransaction(() -> {
                game.save();
                return true;
            });
        } catch (Throwable t) {
            LOG.error("Failed to run transaction: ", t);
            return false;
        }
    }

    public int delete(String id) {
        try {
            JPA.withTransaction(() -> Game.findById(id).delete());
            return 1;
        } catch (Throwable t) {
            LOG.error("Failed to run transaction: ", t);
            return 0;
        }
    }

    public List<Game> listForDashboard() {
        try {
            return JPA.withTransaction(() -> {
                List<Game> games = JPAUtils.findAll(Game.class);
                for(Game game : games) {
                    game.setSummary(new Summary());

                    if (! StringUtils.isEmpty(game.getAwsSqsUrl())) {
                        LOG.debug("retrieving sqs stats for {}...", game.getId());
                        SQSSyncClient sqs = game.getSQSSyncClient();

                        Map<String, String> stats = sqs.readQueueStats();
                        game.getSummary().setNumVisibleMessages(Integer.valueOf(stats.get(SQSSyncClient.ATTR_NUM_VISIBLE_MESSAGES)));
                        game.getSummary().setNumInvisibleMessages(Integer.valueOf(stats.get(SQSSyncClient.ATTR_NUM_INVISIBLE_MESSAGES)));
                    }

                }

                new TransactionDao().fillTransactionSummary(games);
                return games;
            });
        } catch (Throwable t) {
            LOG.error("Failed to run transaction: ", t);
            return Collections.EMPTY_LIST;
        }
    }
}
