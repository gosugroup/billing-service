package services.dao;

import models.Game;
import models.Summary;
import play.db.jpa.JPA;
import services.networks.facebook.FacebookBillingTransaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

/**
 * Created by shuping on 4/16/15.
 */
public class TransactionDao {

    private Game findGame(List<Game> games, String gameId) {
        for (Game game : games) {
            if (gameId.equals(game.getId())) {
                return game;
            }
        }
        return null;
    }

    public void fillTransactionSummary(List<Game> games) {
//        CriteriaBuilder cb = JPA.em().getCriteriaBuilder();
//        CriteriaQuery<FacebookBillingTransaction> cq = cb.createQuery(FacebookBillingTransaction.class);
//        Root<FacebookBillingTransaction> userRoot = cq.from(FacebookBillingTransaction.class);
//        cq.select(userRoot).where(
//                cb.equal(userRoot.get("gameId"), game.getId())
//        );
//        List<FacebookBillingTransaction> results = JPA.em().createQuery(cq).getResultList();

        List<Object[]> results = JPA.em().createNativeQuery(
                "select game_id, 'fb', count(*), sum(delivery_pending) from facebook_billing_transaction group by game_id " +
                "union all " +
                "select game_id, 'ios', count(*), sum(delivery_pending) from apple_billing_transaction group by game_id " +
                "union all " +
                "select game_id, 'gp', count(*), sum(delivery_pending) from google_billing_transaction group by game_id ").getResultList();

        for (Object[] row : results) {
            Game game = findGame(games, (String) row[0]);
            if (game == null) {
                continue;
            }

            switch ((String) row[1]) {
                case "fb":
                    game.getSummary().setTotalFbTransactions(Integer.valueOf(""+row[2]));
                    game.getSummary().setPendingFbTransactions(Integer.valueOf(""+row[3]));
                    break;
                case "ios":
                    game.getSummary().setTotalIosTransactions(Integer.valueOf("" + row[2]));
                    game.getSummary().setPendingIosTransactions(Integer.valueOf("" + row[3]));
                    break;
                case "gp":
                    game.getSummary().setTotalGpTransactions(Integer.valueOf("" + row[2]));
                    game.getSummary().setPendingGpTransactions(Integer.valueOf("" + row[3]));
                    break;
            }
        }

    }

}
