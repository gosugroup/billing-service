package services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import services.exception.BillingApiException;
import services.utils.JsonUtils;

import java.io.IOException;

/**
* Created by shuping on 4/15/15.
*/
public class BillingResponse {

    public static ObjectWriter jsonWriter = JsonUtils.JSON_MAPPER.writerWithType(BillingResponse.class);
    public static ObjectReader jsonReader = JsonUtils.JSON_MAPPER.reader(BillingResponse.class);


    private int code;
    private String message;

    // for jackson instantiation.
    public BillingResponse() {
    }

    public BillingResponse(BillingResult result) {
        this.code = result.getCode();
        this.message = result.name();
    }

    public BillingResponse(BillingResult result, String message) {
        this.code = result.getCode();
        this.message = message;
    }


    public String toJson() {
        try {
            return jsonWriter.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new BillingApiException(e);
        }
    }

    public static BillingResponse fromJson(String json) {
        try {
            return jsonReader.readValue(json);
        } catch (IOException e) {
            throw new BillingApiException(e);
        }
    }



    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
