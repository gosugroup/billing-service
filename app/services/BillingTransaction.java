package services;

import services.utils.Versioned;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by shuping on 3/27/15.
 */
@MappedSuperclass
public abstract class BillingTransaction extends Versioned {

    @Id
    protected String orderId;

    protected String productId;
    protected int    productQuantity = 1;

    protected String gameId;
    protected String userId;

    protected boolean testMode;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;

    protected boolean deliveryPending;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryStarted;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryFinished;



    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Date getDeliveryFinished() {
        return deliveryFinished;
    }

    public void setDeliveryFinished(Date deliveryFinished) {
        this.deliveryFinished = deliveryFinished;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public boolean isDeliveryPending() {
        return deliveryPending;
    }

    public void setDeliveryPending(boolean deliveryPending) {
        this.deliveryPending = deliveryPending;
    }


    public Date getDeliveryStarted() {
        return deliveryStarted;
    }

    public void setDeliveryStarted(Date deliveryStarted) {
        this.deliveryStarted = deliveryStarted;
    }
}
