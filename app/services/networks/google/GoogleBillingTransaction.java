package services.networks.google;

import services.BillingTransaction;

import javax.persistence.*;

/**
 * Created by shuping on 3/27/15.
 *
 * Google Play sample purchase data
 {
     "orderId":"12999763169054705758.1332024401521612",
     "packageName":"air.air.com.gosugroup.battlefrontheroes",
     "productId":"600",
     "purchaseTime":1395910480051,
     "purchaseState":0,
     "developerPayload":"1394771666#1",
     "purchaseToken":"plhjjakkabhnkipkjoffpngj.AO-J1OyZH4wWyUO0f1Aa6Jhiroe9VP_AJm-T3iBtCy1945l0EPRzt-_G9P57-n3_gAR5cb8A7gJ0qh-ooEaknEkmWVn4rSgJWdIikmquCu9Z0DGBP_RpCf6nuKJvQftHDg-Up8GXdiT9"
 }
 *
 */
@Entity
@Table(name = "google_billing_transaction")
public class GoogleBillingTransaction extends BillingTransaction {

    public static final int PURCHASE_STATE_PURCHASED = 0;
    public static final int PURCHASE_STATE_CANCELED = 1;
    public static final int PURCHASE_STATE_REFUNDED = 2;

    // Google's state of the order: 0 (purchased), 1 (canceled), or 2 (refunded).
    private int    purchaseState;
    private String developerPayload;
    private String purchaseToken;


    public int getPurchaseState() {
        return purchaseState;
    }

    public void setPurchaseState(int purchaseState) {
        this.purchaseState = purchaseState;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public void setDeveloperPayload(String developerPayload) {
        this.developerPayload = developerPayload;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public void setPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }

}
