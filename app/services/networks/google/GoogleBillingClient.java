package services.networks.google;


import com.fasterxml.jackson.databind.ObjectReader;
import services.*;
import models.Game;
import org.apache.commons.codec.binary.Base64;
import play.Logger;
import services.exception.DecodeTransactionException;
import services.exception.InvalidTransactionException;
import services.stats.UpsightClient;
import services.utils.JsonUtils;

import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;

public class GoogleBillingClient extends BillingClient <GoogleBillingTransaction> {

    private static final Logger.ALogger LOG = Logger.of(GoogleBillingClient.class);

    private static final ObjectReader jsonReader = JsonUtils.JSON_MAPPER.reader(GoogleBillingTransaction.class);

    // CE key
    //private static final String ENCODED_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk4b9L3spokANBNQEtNEgqaKheDfMlu381wyP0rMLocig5/Jn9W9hnuqiSDyeaDH1uSxnLGiS5/+h+ccW/jWsw+20cdLHdSCDHJstZLWV/vVgGYGGZ2OvkEwWSVkYTMUVms4WDGi4GDGReAysSDM0dOiV7A7V4+lz9wlRT10Q5RONYVMWt/3mz2pRGknNnVhk0PhDLhY91ixepY0M4aZSIKD4gXu+WbBRo+9cZNGdqlgkQ8piDtyihXqJG+p32kjDTWCoysgG759kuvFStUaW31YrVx1TyVbz7bNp2SRcRduqvWMYAILsB1Lg/0WduslsCfxnad98nGSGc+IH6CMNOwIDAQAB";
    // BFH key
    //private static final String ENCODED_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiTBLHoC9TjSjTNqpGv46AqBJWexYZyMDmbzfiL00xaI1jB/b38MFQzV188zEsYwHirDNFvh3W5H4vL2acCQYWqltsK40Y1MMbvVAK+6iweASRTcTlcp/Q0mTkBVAMwNQLkV7fVat4DYJuUoUQhRbFtnnJT87cwF3O+bFer6EraAUZ+ds1HzprcRx8LRjGDuFlxG1W+pMGVOezIoJJM/QcGLSWRLeT5DTPz1boxl49031Mt6CWQnJw2X0WlS4CgAope2+GWOmYDDmWoLGz9sYiaX1TcP0T8JgrkSbWyVbGNvrSh+/w34l5hnfYpqVoUEMQ/fd36LpqVowbG5RV6ewcwIDAQAB";


    public GoogleBillingClient(Game game) {
        super(ClientType.GOOGLE_PLAY, game,
                new UpsightClient(game.isGpUpsightTestServer(), game.getGpUpsightApiKey()),
                GoogleBillingTransaction.class);
    }

    public BillingResponse handlePayment(String userId, String purchaseData, String dataSignature, boolean markDelivered) {
        final GoogleBillingTransaction tx;
        try {
            tx = decodeTransaction(purchaseData, dataSignature);
        } catch (DecodeTransactionException | InvalidTransactionException e) {
            LOG.error("Failed to validate transaction: ", e);
            return BillingResult.INVALID_TRANSACTION.toBillingResponse();
        }

        if (tx.getPurchaseState() != GoogleBillingTransaction.PURCHASE_STATE_PURCHASED) {
            LOG.error("Not implemented operation for purchase state: {}", tx.getPurchaseState());
            return BillingResult.FAILED.toBillingResponse("purchaseState is not purchased");
        }

        tx.setUserId(userId);
        tx.setGameId(game.getId());
        tx.setCreatedTime(new Date());

        return handlePayment(tx, markDelivered, true);
    }


    private GoogleBillingTransaction decodeTransaction(String purchaseData, String dataSignature) throws DecodeTransactionException, InvalidTransactionException {
        try {
            LOG.debug("json: {}", purchaseData);
            LOG.debug("signature: {}", dataSignature);

            if (isValidTransaction(purchaseData, dataSignature)) {
                return jsonReader.readValue(purchaseData);
            }
        } catch (IOException e) {
            throw new DecodeTransactionException(e);
        }
        throw new InvalidTransactionException("This transaction is not valid: " + purchaseData +
                " with signature: " + dataSignature);
    }

    private boolean isValidTransaction(String transaction, String signature) throws DecodeTransactionException {
        try {
            PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(game.getGpAppPublicKey())));

            Signature sig = Signature.getInstance("SHA1withRSA");
            sig.initVerify(publicKey);
            sig.update(transaction.getBytes());
            return sig.verify(Base64.decodeBase64(signature));
        } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException | SignatureException e) {
            throw new DecodeTransactionException(e);
        }
    }
}
