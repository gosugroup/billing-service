package services.networks.apple;

import com.fasterxml.jackson.databind.ObjectReader;
import services.*;
import models.Game;
import play.Logger;
import play.libs.ws.WS;
import services.exception.DecodeTransactionException;
import services.exception.InvalidTransactionException;
import services.networks.apple.receipt.AppleReceipt;
import services.networks.apple.receipt.MultiReceiptHolder;
import services.networks.apple.receipt.SingleReceipt;
import services.stats.UpsightClient;
import services.utils.JsonUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by shuping on 3/27/15.
 */
public class AppleBillingClient extends BillingClient<AppleBillingTransaction> {

    private static Logger.ALogger LOG = Logger.of(AppleBillingClient.class);

    private static final ObjectReader singleReceiptReader = JsonUtils.JSON_MAPPER.reader(SingleReceipt.class);
    private static final ObjectReader multiReceiptReader = JsonUtils.JSON_MAPPER.reader(MultiReceiptHolder.class);

    private static final String APPLE_SANDBOX_URL = "https://sandbox.itunes.apple.com/verifyReceipt";
    private static final String APPLE_LIVE_URL = "https://buy.itunes.apple.com/verifyReceipt";


    public AppleBillingClient (Game game) {
        super(ClientType.IOS, game,
                new UpsightClient(game.isIosUpsightTestServer(), game.getIosUpsightApiKey()),
                AppleBillingTransaction.class);
    }

    public BillingResponse handlePayment(String userId, String transactionId, String encodedReceipt, boolean markDelivered) {
        final AppleBillingTransaction tx;
        try {
            tx = validateTransaction(encodedReceipt, transactionId);
        } catch (DecodeTransactionException | InvalidTransactionException e) {
            LOG.error("Failed to validate transaction: ", e);
            return BillingResult.INVALID_TRANSACTION.toBillingResponse();
        }

        tx.setUserId(userId);
        tx.setGameId(game.getId());
        tx.setTestMode(game.isIosTestMode());
        tx.setCreatedTime(new Date());

        return handlePayment(tx, markDelivered, !game.isProductionMode() || !game.isIosTestMode());
    }


    private AppleBillingTransaction validateTransaction(String encodedReceipt, String tid)
            throws DecodeTransactionException, InvalidTransactionException {

        try {
            final boolean oldReceipt = !encodedReceipt.startsWith("N_");
            final String _receipt = (oldReceipt) ? encodedReceipt : encodedReceipt.substring(2);
            final String json =
                    "{\"receipt-data\":\"" + (oldReceipt ? new String(Base64.getEncoder().encode(
                            _receipt.getBytes("UTF-8"))) : _receipt) + "\"}";

            // TODO: technically it's strongly discouraged to use Promise.get() since it turns this code into blocking,
            // which is against Play async design and is bad for performance.
            final InputStream in = WS.url(game.isIosTestMode() ? APPLE_SANDBOX_URL : APPLE_LIVE_URL)
                    .setContentType("application/json")
                    .post(json)
                    .get(10, TimeUnit.SECONDS)
                    .getBodyAsStream();

            final AppleReceipt r = oldReceipt ? singleReceiptReader.readValue(in) : multiReceiptReader.readValue(in);
            LOG.debug("Apple verify response: {}", r.getStatus());

            if (! r.isValid(tid)) {
                // https://developer.apple.com/library/ios/releasenotes/General/ValidateAppStoreReceipt/Chapters/ValidateRemotely.html
                throw new InvalidTransactionException("Apple verify status:" + r.getStatus());
            }
            return r.getTransaction();

        } catch (IOException e) {
            throw new DecodeTransactionException(e);
        }
    }
}
