package services.networks.apple;

import com.fasterxml.jackson.annotation.JsonProperty;
import services.BillingTransaction;

import javax.persistence.*;

/**
 * Created by shuping on 3/27/15.
 *
 * Apple sample response from verify service:
 *
 {"receipt":{
     "original_purchase_date_pst":"2014-03-31 02:08:54 America/Los_Angeles",
     "purchase_date_ms":"1396256934030",
     "unique_identifier":"65b72d5eef472b0676782af7e21b08a190b3b86e",
     "original_transaction_id":"1000000106249549",
     "bvrs":"0.1.1",
     "transaction_id":"1000000106249549",
     "quantity":"1",
     "unique_vendor_identifier":"60B52040-F4D9-4576-8B3E-0291B9770676",
     "item_id":"847338299",
     "product_id":"602",
     "purchase_date":"2014-03-31 09:08:54 Etc/GMT",
     "original_purchase_date":"2014-03-31 09:08:54 Etc/GMT",
     "purchase_date_pst":"2014-03-31 02:08:54 America/Los_Angeles",
     "bid":"air.com.gosugroup.battlefrontheroes",
     "original_purchase_date_ms":"1396256934030"
 },
 "status":0
 }
 */
@Entity
@Table(name = "apple_billing_transaction")
public class AppleBillingTransaction extends BillingTransaction {

    /**
     * Some Json property names are different so need to specify them here.
     * @param orderId
     * @param productId
     * @param productQuantity
     */
    public AppleBillingTransaction(
            @JsonProperty("transaction_id") String orderId,
            @JsonProperty("product_id") String productId,
            @JsonProperty("quantity") int productQuantity) {

        this.orderId = orderId;
        this.productId = productId;
        this.productQuantity = productQuantity;
    }

}
