/*
 * Copyright 2014 Gosu Group Ltd
 */
package services.networks.apple.receipt;

import com.fasterxml.jackson.annotation.JsonProperty;
import services.networks.apple.AppleBillingTransaction;

import java.util.ArrayList;
import java.util.List;

/**
 * User: acunha Date: 02/12/14. Time: 5:20 PM
 */
public class MultiReceiptHolder extends AppleReceipt {

  private final List<AppleBillingTransaction> transactions;
  private AppleBillingTransaction transaction;

  public MultiReceiptHolder(@JsonProperty("status") int status,
                            @JsonProperty("receipt") MultiReceipt receipt) {
    super(status);
    this.transactions = receipt != null ? receipt.getTransaction() : new ArrayList<>();
  }

  @Override
  protected boolean hasTransaction(String transaction) {
    this.transaction =
        transactions.stream()
            .filter(t -> t.getOrderId().equals(transaction))
            .findFirst()
            .orElse(null);
    return this.transaction != null;
  }

  public AppleBillingTransaction getTransaction() {
    return this.transaction;
  }

  @Override
  public String toString() {
    return "MultiReceiptHolder{" +
           ", transactions=" + transactions +
           '}';
  }
}
