/*
 * Copyright 2014 Gosu Group Ltd
 */

package services.networks.apple.receipt;

import com.fasterxml.jackson.annotation.JsonProperty;
import services.networks.apple.AppleBillingTransaction;

import java.util.Arrays;
import java.util.List;

/**
 * Created by acunha on 4/1/14.
 */
public class MultiReceipt {
    private final List<AppleBillingTransaction> transactions;

    public MultiReceipt(@JsonProperty("in_app") AppleBillingTransaction[] transactions) {
        this.transactions = Arrays.asList(transactions);
    }

    public List<AppleBillingTransaction> getTransaction() {
        return transactions;
    }

    @Override
    public String toString() {
        return "MultiReceipt{" +
               ", transactions=" + transactions +
               '}';
    }
}
