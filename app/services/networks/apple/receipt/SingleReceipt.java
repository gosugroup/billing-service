package services.networks.apple.receipt;

import com.fasterxml.jackson.annotation.JsonProperty;
import services.networks.apple.AppleBillingTransaction;

public class SingleReceipt extends AppleReceipt {
    private final AppleBillingTransaction transaction;

    public SingleReceipt(@JsonProperty("receipt") AppleBillingTransaction transaction,
                         @JsonProperty("status") int status) {
        super(status);
        this.transaction = transaction;
    }

    @Override
    protected boolean hasTransaction(String transaction) {
        return true;
    }

    public AppleBillingTransaction getTransaction() {
        return transaction;
    }
}
