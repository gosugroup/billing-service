/*
 * Copyright 2014 Gosu Group Ltd
 */

package services.networks.apple.receipt;

import services.networks.apple.AppleBillingTransaction;

/**
 * User: acunha Date: 02/12/14. Time: 5:49 PM
 */
public abstract class AppleReceipt {
  private final int status;

  protected AppleReceipt(int status) {
    this.status = status;
  }

  protected abstract boolean hasTransaction(String transaction);

  public final boolean isValid(String transaction) {
    return status == 0 && hasTransaction(transaction);
  }

  public final int getStatus() {return status;}

  public abstract AppleBillingTransaction getTransaction();


}
