package services.networks.apple.receipt;

import com.fasterxml.jackson.annotation.JsonProperty;
import services.networks.apple.AppleBillingTransaction;

/**
 * Created by acunha on 4/1/14.
 */
public class Receipt {
    private final int status;
    private final AppleBillingTransaction transaction;

    public Receipt(@JsonProperty("receipt")AppleBillingTransaction transaction, @JsonProperty("status")int status) {
        this.status = status;
        this.transaction = transaction;
    }

    public int getStatus() {
        return status;
    }

    public AppleBillingTransaction getTransaction() {
        return transaction;
    }
}
