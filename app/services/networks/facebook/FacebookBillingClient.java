package services.networks.facebook;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import models.Game;
import play.Logger;
import play.db.jpa.JPA;
import services.*;
import services.aws.SESAsyncClient;
import services.aws.SQSException;
import services.aws.SQSSyncClient;
import services.exception.BillingException;
import services.networks.facebook.graph.FacebookCall;
import services.networks.facebook.graph.PaymentSignedRequest;
import services.networks.facebook.graph.payment.*;
import services.products.Product;
import services.stats.BillingEvent;
import services.stats.BillingEventType;
import services.stats.UpsightClient;
import services.utils.*;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

public class FacebookBillingClient extends BillingClient<FacebookBillingTransaction> {

    private static final Logger.ALogger LOG = Logger.of(FacebookBillingClient.class);

    public static final ObjectReader paymentSignedRequestReader = JsonUtils.JSON_MAPPER.reader(PaymentSignedRequest.class);
    public static final ObjectWriter notificationWriter = JsonUtils.JSON_MAPPER.writerWithType(FacebookBillingNotification.class);

    private FacebookCall facebookCall;


    // for easy testing.
    protected FacebookBillingClient(Game game, FacebookCall facebookCall) {
        super(ClientType.WEB, game,
                new UpsightClient(game.isFbUpsightTestServer(), game.getFbUpsightApiKey()),
                FacebookBillingTransaction.class);
        this.facebookCall = facebookCall;
    }


    public FacebookBillingClient(Game game) {
        this(game, new FacebookCall(game.getFbAppId(), game.getFbAppSecret()));
    }


    /**
     * @return BillingResult.INSERTED or BillingResult.NO_ACTION (already processed before);
     */
    public BillingResult processPaymentUpdate(String transactionId) {

        Payment payment;

        try {
            payment = facebookCall.getFacebookPayment(transactionId);
        } catch (IOException ex) {
            LOG.error("Cannot get the billing details for transaction {}, exception: {}", transactionId, ex);
            return BillingResult.FAILED;
        }

        List<Payment.Action> actions = Optional.ofNullable(payment.getActions()).orElse(Collections.EMPTY_LIST);
        for (Payment.Action action : actions) {
            if (action.getStatus() != ActionStatus.COMPLETED) {
                continue;
            }
            try {
                BillingEvent event = JPA.withTransaction(() -> processPaymentAction(payment, action));
                sendBillingEvent(event);
            } catch (Throwable t) {
                LOG.error("Failed to process payment action: {}, error: {}", payment, t);
                return BillingResult.FAILED;
            }
        }

        List<Payment.Dispute> disputes = Optional.ofNullable(payment.getDisputes()).orElse(Collections.EMPTY_LIST);
        for (Payment.Dispute dispute : disputes) {
            try {
                JPA.withTransaction(() -> processPaymentDispute(payment, dispute));
            } catch (Throwable t) {
                LOG.error("Failed to process payment dispute: {}, error: {}", payment, t);
                return BillingResult.FAILED;
            }
        }

        return BillingResult.OK;
    }


    private BillingEvent processPaymentAction(Payment payment, Payment.Action action) throws BillingException {
        LOG.debug("Action: orderId={}, type={}, status={}", payment.getId(), action.getType(), action.getStatus());
        EntityManager em = JPA.em();
        FacebookBillingTransaction tx = em.find(FacebookBillingTransaction.class, payment.getId(), LockModeType.OPTIMISTIC);

        ActionType actionType = action.getType();
        BillingEventType eventType = mapToBillingEventType(actionType);
        if (tx == null && actionType == ActionType.CHARGE) {
            // do charge.
            LOG.debug("Charge the new action...");
            tx = constructNewTransaction(payment, action);
            provisionPaymentAction(tx, payment, action, null);
            tx.setPurchaseEventSent(true);
            tx.setDeliveryPending(false);
            em.persist(tx);
            return createPaymentBillingEvent(payment, tx, eventType);

        } else if (tx != null && actionType == ActionType.CHARGE) {
            // it's already charged using signed request, now we add missing info here.
            LOG.debug("Charge the already processed action...");
            tx.setTestMode(payment.isTest());
            tx.setExchangeRate(payment.getExchangeRate());
            if (! tx.isPurchaseEventSent()) {
                tx.setPurchaseEventSent(true);
                return createPaymentBillingEvent(payment, tx, eventType);
            }
            return null; // otherwise don't send the purchase event again.

        } else if (tx != null && tx.getStatus() == FacebookBillingTransactionStatus.CHARGE
                && (actionType == ActionType.CHARGEBACK || actionType == ActionType.REFUND || actionType == ActionType.DECLINE)) {
            // chargeback, facebook initiated refund, or decline.
            LOG.debug("retract the charge...");
            tx.setStatus(actionType.name());
            provisionPaymentAction(tx, payment, action, FacebookBillingTransactionStatus.CHARGE);
            return createPaymentBillingEvent(payment, tx, eventType);

        } else if (tx != null && tx.getStatus() == FacebookBillingTransactionStatus.CHARGEBACK
                && actionType == ActionType.CHARGEBACK_REVERSAL) {
            // chargeback reversal.
            LOG.debug("reverse the charge back...");
            tx.setStatus(actionType.name());
            provisionPaymentAction(tx, payment, action, FacebookBillingTransactionStatus.CHARGEBACK);
            return createPaymentBillingEvent(payment, tx, eventType);
        }

        return null;
    }

    private void processPaymentDispute(Payment payment, Payment.Dispute dispute) throws BillingException {
        LOG.debug("Dispute: user={}, status={}, reason={}", dispute.getUserEmail(),
                dispute.getStatus(), dispute.getReason());

        FacebookBillingTransaction tx = JPA.em().find(FacebookBillingTransaction.class, payment.getId(), LockModeType.OPTIMISTIC);
        if (tx == null) {
            throw new BillingException("Transaction not found in billing db: " + payment.getId());
        }

        FacebookBillingTransactionStatus previousStatus = tx.getStatus();
        DisputeStatus disputeStatus = dispute.getStatus();

        if ((tx.getStatus() == FacebookBillingTransactionStatus.CHARGE ||
                tx.getStatus() == FacebookBillingTransactionStatus.CHARGEBACK_REVERSAL) &&
                disputeStatus == DisputeStatus.PENDING) {

            FacebookUserDispute userDispute = tx.getDisputes().isEmpty() ? null : tx.getDisputes().get(0);
            if (userDispute == null) {
                // special case for dispute, we save (and commit) it before we start to process it
                // so that we know there is a pending dispute regardless its status.
                userDispute = new FacebookUserDispute();
                userDispute.setOrderId(tx.getOrderId());
                userDispute.setEmail(dispute.getUserEmail());
                userDispute.setComment(dispute.getUserComment());
                userDispute.setCreatedTime(dispute.getTimeCreated());
                userDispute.setReason(dispute.getReason());
                userDispute.setStatus(dispute.getStatus());

                tx.getDisputes().add(userDispute);
            }

            // do user initiated refund.
            LOG.debug("Start to refund user: {}", dispute.getUserEmail());
            tx.setStatus(FacebookBillingTransactionStatus.DISPUTE);
            provisionPaymentDispute(tx, payment, dispute, previousStatus);
        }
    }


    private BillingEventType mapToBillingEventType(ActionType type) {
        BillingEventType event = null;
        switch (type) {
            case CHARGE:
                return BillingEventType.PURCHASE;
            case CHARGEBACK:
                return BillingEventType.CHARGEBACK;
            case REFUND:
                return BillingEventType.REFUND;
            case DECLINE:
                return BillingEventType.DECLINE;
            case CHARGEBACK_REVERSAL:
                return BillingEventType.CHARGEBACK_REV;
        }
        return event;
    }

    private FacebookBillingTransaction constructNewTransaction(Payment payment, Payment.Action action) {
        if (payment.getItems().size() != 1) {
            throw new IllegalArgumentException("Can't deal with 0 or more than 1 products in items array.");
        }

        FacebookBillingTransaction tx = new FacebookBillingTransaction();
        tx.setOrderId(payment.getId());
        tx.setGameId(game.getId());
        tx.setUserId(payment.getUser().getId());

        final Payment.Item item = payment.getItems().get(0);
        tx.setProductId(item.getId().substring(item.getId().lastIndexOf("/") + 1));
        tx.setProductQuantity(item.getQuantity());
        tx.setStatus(action.getType().name());
        tx.setTestMode(payment.isTest());
        tx.setCurrency(action.getCurrency());
        tx.setPrice(action.getAmount());
        tx.setExchangeRate(payment.getExchangeRate());
        tx.setPurchaseEventSent(false);

        return tx;
    }


    private void provisionPaymentAction(FacebookBillingTransaction tx, Payment payment, Payment.Action action,
                                        FacebookBillingTransactionStatus dependencyStatus) throws BillingException {
        LOG.debug("provisionPaymentAction: txId={}, txStatus={}", tx.getOrderId(), tx.getStatus());
        FacebookBillingNotification notification = new FacebookBillingNotification(
            tx, payment, action, dependencyStatus);
        sendProvisionNotification(notification);
    }

    private void provisionPaymentDispute(FacebookBillingTransaction tx, Payment payment, Payment.Dispute dispute,
                                         FacebookBillingTransactionStatus dependencyStatus) throws BillingException {
        LOG.debug("provisionPaymentDispute: txId={}, txStatus={}", tx.getOrderId(), tx.getStatus());
        FacebookBillingNotification notification = new FacebookBillingNotification(
            tx, payment, dispute, dependencyStatus);
        sendProvisionNotification(notification);
    }

    private void sendProvisionNotification(FacebookBillingNotification notification) throws BillingException {
        try {
            String json = notificationWriter.writeValueAsString(notification);
            LOG.debug("sending event to sqs queue: {}", json);

            SQSSyncClient sqs = game.getSQSSyncClient();
            sqs.sendMessage(json);

        } catch (JsonProcessingException e) {
            throw new BillingException(e);
        } catch (SQSException e) {
            throw new BillingException(e);
        }
    }

    private BillingEvent createPaymentBillingEvent(Payment payment, FacebookBillingTransaction tx, BillingEventType eventType) {
        final float price = (float) (tx.getPrice() * payment.getExchangeRate());
        final String sUid = tx.getUserId();
        final String itemCode = tx.getProductId();
        return new BillingEvent(eventType, ClientType.WEB, sUid, itemCode, price, tx.isTestMode());
    }

    private BillingEvent createDisputeBillingEvent(FacebookBillingTransaction tx, FacebookUserDispute dispute) {
        final float price = (float) (dispute.getRefundedAmount() * tx.getExchangeRate());
        final String sUid = tx.getUserId();
        final String itemCode = tx.getProductId();
        return new BillingEvent(BillingEventType.DISPUTE, ClientType.WEB, sUid, itemCode, price, tx.isTestMode());
    }

    private void sendBillingEvent(BillingEvent event) {
        if (event != null &&
                (!game.isProductionMode() || !event.isTestMode())) {
            upsight.sendBillingEvent(event.getClientType(), event.getItemCode(), event.getUsdPrice(), event.getUserId(), event.getEventType(), null);
        }
    }

    public BillingResponse processSignedRequest(String signedRequestString, boolean markDelivered) {

        final FacebookBillingTransaction tx;

        try {
            PaymentSignedRequest signedRequest = decodeSignedRequest(signedRequestString, game.getFbAppSecret());
            LOG.debug("Decoded signed request: {}", signedRequest);

            if (signedRequest.getStatus() != ActionStatus.COMPLETED) {
                throw new BillingException("Payment in signed request is not completed.");
            }

            String productId = signedRequest.getProductId();
            Product product = game.getProductFactory().getProduct(productId);
            if (product == null) {
                throw new BillingException("Wrong product id: " + productId);
            }
            double expectedPrice = product.getPrice(signedRequest.getCurrency()) * signedRequest.getQuantity();
            if (! NumberUtils.equals(signedRequest.getAmount(), expectedPrice)) {
                throw new BillingException("Wrong price for product " + productId + " in " + signedRequest.getCurrency()
                        + ", expected: " + expectedPrice + ", actual: " + signedRequest.getAmount());
            }

            tx = new FacebookBillingTransaction();
            tx.setOrderId(signedRequest.getPaymentId());
            tx.setUserId(signedRequest.getUserId());
            tx.setGameId(game.getId());
            tx.setCurrency(signedRequest.getCurrency());
            tx.setPrice(signedRequest.getAmount());
            tx.setProductId(signedRequest.getProductId());
            tx.setProductQuantity(signedRequest.getQuantity());
            tx.setStatus(FacebookBillingTransactionStatus.CHARGE);
            tx.setPurchaseEventSent(false);
            // Signed request doesn't provide exchange rate and test mode information,
            // we will update transaction with these info when payment update arrives.

        } catch (BillingException t) {
            LOG.error("Failed to process signed request: ", t);
            return BillingResult.FAILED.toBillingResponse(t);
        }

        // we don't send purchase event when processing signed request.
        // Instead we send them later when handing realtime updates.
        return handlePayment(tx, markDelivered, false);

    }

    protected PaymentSignedRequest decodeSignedRequest(String signedRequestString, String secret) throws BillingException {
        if (signedRequestString == null) {
            throw new BillingException("signed request is null");
        }

        int idx = signedRequestString.indexOf('.');
        if (idx < 0) {
            throw new BillingException("signed request is in bad format");
        }

        try {
            byte[] signature = StringUtils.urlAndBase64Decode(signedRequestString.substring(0, idx));
            String payload = signedRequestString.substring(idx + 1);

            String HMACSHA256 = "HMACSHA256";
            SecretKeySpec keySpec = new SecretKeySpec(secret.getBytes("UTF-8"), HMACSHA256);
            Mac mac = Mac.getInstance(HMACSHA256);
            mac.init(keySpec);
            byte[] expectedSignature = mac.doFinal(payload.getBytes("UTF-8"));

            if (!Arrays.equals(signature, expectedSignature)) {
                throw new BillingException("signed request with wrong signature");
            }

            byte[] decodedPayload = StringUtils.urlAndBase64Decode(payload);
            PaymentSignedRequest signedRequest = paymentSignedRequestReader.readValue(decodedPayload);
            return signedRequest;

        } catch (IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new BillingException(e);
        }
    }

    public BillingResponse resolvePaymentDispute(String orderId, String refundCurrency, double refundAmount,
                                      String emailTitle, String emailBody) {
        try {
            return JPA.withTransaction(() -> {
                final FacebookBillingTransaction tx = JPA.em().find(FacebookBillingTransaction.class, orderId);
                if (tx == null) {
                    LOG.error("Transaction doesn't exists: {}", orderId);
                    return BillingResult.NOT_EXISTS.toBillingResponse();
                }

                final FacebookUserDispute dispute = JPA.em().find(FacebookUserDispute.class, orderId);
                if (dispute == null || dispute.getStatus() != DisputeStatus.PENDING) {
                    LOG.error("Dispute doesn't exists, or it's not still pending for transaction: {}", orderId);
                    return BillingResult.NOT_EXISTS.toBillingResponse();
                }

                if (refundAmount > 0) {
                    facebookCall.refundFacebookPayment(orderId, refundCurrency, refundAmount);

                    dispute.setStatus(DisputeStatus.RESOLVED);
                    dispute.setReason(DisputeReason.REFUNDED_IN_CASH);
                    dispute.setRefundedCurrency(refundCurrency);
                    dispute.setRefundedAmount(refundAmount);

                    BillingEvent billingEvent = createDisputeBillingEvent(tx, dispute);
                    sendBillingEvent(billingEvent);

                    sendDisputeEmailToPlayer(tx, dispute, emailTitle, emailBody);

                } else {
                    facebookCall.resolveFacebookDispute(orderId, DisputeReason.DENIED_REFUND);

                    dispute.setStatus(DisputeStatus.RESOLVED);
                    dispute.setReason(DisputeReason.DENIED_REFUND);

                    sendDisputeEmailToPlayer(tx, dispute, emailTitle, emailBody);
                }
                return BillingResult.RESOLVED.toBillingResponse();
            });

        } catch (Throwable t) {
            LOG.error("Failed to resolve dispute: ", t);
            return BillingResult.FAILED.toBillingResponse(t);
        }
    }

    protected void sendDisputeEmailToPlayer(FacebookBillingTransaction tx, FacebookUserDispute dispute,
                                            String emailSubject, String emailBody) {
        LOG.debug("Sending dispute email to player: {}", dispute.getEmail());
        if (emailSubject == null || emailBody == null) {
            LOG.error("Can't send email with null subject or body");
            return;
        }

        SESAsyncClient ses = game.getSESAsyncClient();
        ses.sendHtmlEmail(game.getEmailSender(), new String[]{dispute.getEmail()},
                new String[]{game.getEmailRecipient()}, emailSubject, emailBody);
    }

}