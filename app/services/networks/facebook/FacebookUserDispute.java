package services.networks.facebook;


import services.utils.Versioned;
import services.networks.facebook.graph.payment.DisputeReason;
import services.networks.facebook.graph.payment.DisputeStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "facebook_billing_dispute")
public class FacebookUserDispute extends Versioned {

    @Id
    private String orderId;

    private String email;
    private String comment;

    @Enumerated(EnumType.STRING)
    private DisputeStatus status;
    @Enumerated(EnumType.STRING)
    private DisputeReason reason;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    private String refundedCurrency;
    private double refundedAmount;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public DisputeStatus getStatus() {
        return status;
    }

    public void setStatus(DisputeStatus status) {
        this.status = status;
    }

    public DisputeReason getReason() {
        return reason;
    }

    public void setReason(DisputeReason reason) {
        this.reason = reason;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getRefundedCurrency() {
        return refundedCurrency;
    }

    public void setRefundedCurrency(String refundedCurrency) {
        this.refundedCurrency = refundedCurrency;
    }

    public double getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

}
