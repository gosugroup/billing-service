package services.networks.facebook;

import services.BillingTransaction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "facebook_billing_transaction")
public class FacebookBillingTransaction extends BillingTransaction {

    private double price;
    private String currency;
    private double exchangeRate;

    // has sent the purchase billing event for this transaction?
    private boolean purchaseEventSent;


    @Enumerated(EnumType.STRING)
    private FacebookBillingTransactionStatus status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderId")
    private List<FacebookUserDispute> disputes = new ArrayList<>();


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public FacebookBillingTransactionStatus getStatus() {
        return status;
    }

    public void setStatus(FacebookBillingTransactionStatus status) {
        this.status = status;
    }

    public void setStatus(String status) {
        this.status = FacebookBillingTransactionStatus.valueOf(status);
    }

    public List<FacebookUserDispute> getDisputes() {
        return disputes;
    }

    public void setDisputes(List<FacebookUserDispute> disputes) {
        this.disputes = disputes;
    }

    public boolean isPurchaseEventSent() {
        return purchaseEventSent;
    }

    public void setPurchaseEventSent(boolean purchaseEventSent) {
        this.purchaseEventSent = purchaseEventSent;
    }
}
