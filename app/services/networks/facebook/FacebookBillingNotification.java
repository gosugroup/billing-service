package services.networks.facebook;

import com.fasterxml.jackson.annotation.JsonProperty;
import services.networks.facebook.graph.payment.Payment;

import java.util.Date;

/**
 * Created by shuping on 4/13/15.
 */
public class FacebookBillingNotification {

    private final String version;

    @JsonProperty(value="notification_id")
    private final String notificationId;
    @JsonProperty(value="dependency_id")
    private final String dependencyId;

    @JsonProperty(value="time_created")
    private final Date timeCreated;

    @JsonProperty(value="game_id")
    private final String gameId;
    @JsonProperty(value="user_id")
    private final String userId;
    @JsonProperty(value="order_id")
    private final String orderId;
    @JsonProperty(value="action")
    private final String action;
    @JsonProperty(value="product_id")
    private final String productId;
    @JsonProperty(value="product_quantity")
    private final int productQuantity;
    @JsonProperty(value="currency")
    private final String currency;
    @JsonProperty(value="amount")
    private final double amount;
    @JsonProperty(value="exchange_rate")
    private final double exchangeRate;

    @JsonProperty(value="user_email")
    private final String userEmail;
    @JsonProperty(value="user_comment")
    private final String userComment;

    public FacebookBillingNotification (FacebookBillingTransaction tx, Payment payment, Payment.Action action,
                                        FacebookBillingTransactionStatus dependencyStatus) {
        this.version = "0.1";

        this.notificationId = tx.getOrderId() + "." +  tx.getStatus();
        this.dependencyId = dependencyStatus == null ? null : tx.getOrderId() + "." + dependencyStatus;

        this.timeCreated = action.getTimeCreated();

        this.gameId = tx.getGameId();
        this.userId = tx.getUserId();
        this.orderId = tx.getOrderId();
        this.action = action.getType().name();
        this.productId = tx.getProductId();
        this.productQuantity = tx.getProductQuantity();
        this.currency = action.getCurrency();
        this.amount = action.getAmount();
        this.exchangeRate = tx.getExchangeRate();

        this.userEmail = null;
        this.userComment = null;
    }

    public FacebookBillingNotification (FacebookBillingTransaction tx, Payment payment, Payment.Dispute dispute,
                                        FacebookBillingTransactionStatus dependencyStatus) {
        this.version = "0.1";

        this.notificationId = tx.getOrderId() + "." +  "DISPUTE";
        this.dependencyId = dependencyStatus == null ? null : tx.getOrderId() + "." + dependencyStatus;

        this.timeCreated = dispute.getTimeCreated();

        this.gameId = tx.getGameId();
        this.userId = tx.getUserId();
        this.orderId = tx.getOrderId();
        this.action = "DISPUTE";
        this.productId = tx.getProductId();
        this.productQuantity = tx.getProductQuantity();
        this.currency = payment.getRefundableAmount().getCurrency();
        this.amount = payment.getRefundableAmount().getAmount();
        this.exchangeRate = tx.getExchangeRate();

        this.userEmail = dispute.getUserEmail();
        this.userComment = dispute.getUserComment();
    }


    public String getVersion() {
        return version;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public String getDependencyId() {
        return dependencyId;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public String getGameId() {
        return gameId;
    }

    public String getUserId() {
        return userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getAction() {
        return action;
    }

    public String getProductId() {
        return productId;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public String getCurrency() {
        return currency;
    }

    public double getAmount() {
        return amount;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserComment() {
        return userComment;
    }
}
