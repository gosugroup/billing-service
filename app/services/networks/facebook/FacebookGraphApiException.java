package services.networks.facebook;

/**
 * Created by shuping on 4/3/15.
 */
public class FacebookGraphApiException extends Exception {

    public FacebookGraphApiException(String message) {
        super(message);
    }
}
