
package services.networks.facebook;


public enum FacebookBillingTransactionStatus {
    CHARGE ("charge"),
    FAILED ("failed"),
    CHARGEBACK ("chargeback"),
    CHARGEBACK_REVERSAL ("chargeback_reversal"),
    REFUND ("refund"),
    DECLINE ("decline"),
    DISPUTE ("dispute"),
    ;

    private String value;

    private FacebookBillingTransactionStatus(String value) {
        this.value = value;
    }

    public static FacebookBillingTransactionStatus fromValue(String v) {
        for (FacebookBillingTransactionStatus myEnum : values()) {
            if (myEnum.value.equals(v)) {
                return myEnum;
            }
        }
        throw new IllegalArgumentException("invalid string value passed: " + v);
    }
}
