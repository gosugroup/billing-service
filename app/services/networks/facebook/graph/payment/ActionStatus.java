package services.networks.facebook.graph.payment;


import com.fasterxml.jackson.annotation.JsonCreator;

public enum ActionStatus {
    _UNRECOGNIZED_ ("for any other unsupported value"),
    INITIATED ("initiated"),
    COMPLETED ("completed"),
    FAILED ("failed"),
    PENDING ("pending"),

    ;

    private String value;

    private ActionStatus(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ActionStatus fromValue(String v) {
        for (ActionStatus myEnum : values()) {
            if (myEnum.value.equals(v)) {
                return myEnum;
            }
        }
        return _UNRECOGNIZED_;
    }
}
