package services.networks.facebook.graph.payment;


import com.fasterxml.jackson.annotation.JsonCreator;

public enum ActionType {
    _UNRECOGNIZED_ ("for any other unsupported value"),
    CHARGE ("charge"),
    CHARGEBACK("chargeback"),
    CHARGEBACK_REVERSAL("chargeback_reversal"),
    REFUND ("refund"),
    DECLINE("decline"),
    ;
    private String value;

    private ActionType(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ActionType fromValue(String v) {
        for (ActionType myEnum : values()) {
            if (myEnum.value.equals(v)) {
                return myEnum;
            }
        }
        return _UNRECOGNIZED_;
    }
}
