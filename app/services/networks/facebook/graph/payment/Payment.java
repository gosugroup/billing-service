package services.networks.facebook.graph.payment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectReader;
import services.utils.JsonUtils;

import java.util.Date;
import java.util.List;

public class Payment {

    private String id;
    private User user;
    private Application application;
    private List<Action> actions;
    @JsonProperty("refundable_amount")
    private RefundableAmount refundableAmount;
    private List<Dispute> disputes;
    private List<Item> items;
    private String country;
    @JsonProperty("created_time")
    private String createdTime;
    @JsonProperty("payout_foreign_exchange_rate")
    private double exchangeRate;
    private int test;

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Application getApplication() {
        return application;
    }

    public List<Action> getActions() {
        return actions;
    }

    public RefundableAmount getRefundableAmount() {
        return refundableAmount;
    }

    public List<Dispute> getDisputes() {
        return disputes;
    }
    
    public List<Item> getItems() {
        return items;
    }

    public String getCountry() {
        return country;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public boolean isTest() {
        return test == 1;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id='" + id + '\'' +
                ", user=" + user +
                ", application=" + application +
                ", actions=" + actions +
                ", disputes=" + disputes +
                ", refundableAmount=" + refundableAmount +
                ", items=" + items +
                ", country='" + country + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", exchangeRate=" + exchangeRate +
                ", test=" + test +
                '}';
    }

    public static class Dispute {

        DisputeStatus status;
        DisputeReason reason;
        @JsonProperty("user_email")
        String userEmail;
        @JsonProperty("user_comment")
        String userComment;
        @JsonProperty("time_created")
        Date timeCreated;

        public DisputeStatus getStatus() {
            return status;
        }

        public void setStatus(DisputeStatus status) {
            this.status = status;
        }

        public DisputeReason getReason() {
            return reason;
        }

        public void setReason(DisputeReason reason) {
            this.reason = reason;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public String getUserComment() {
            return userComment;
        }

        public void setUserComment(String userComment) {
            this.userComment = userComment;
        }

        public Date getTimeCreated() {
            return timeCreated;
        }

        public void setTimeCreated(Date timeCreated) {
            this.timeCreated = timeCreated;
        }
        
        @Override
        public String toString() {
            return "Dispute{" + "status=" + status + ", reason=" + reason + ", userEmail=" + userEmail + ", userComment=" + userComment + ", timeCreated=" + timeCreated + '}';
        }
    }

    public static class RefundableAmount {

        String currency;
        double amount;
        
        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }
        
        @Override
        public String toString() {
            return "RefundableAmount{" + "currency=" + currency + ", amount=" + amount + '}';
        }
    }
    

    public static class Item {
        String type;
        String id;
        int quantity;

        public String getType() {
            return type;
        }

        public String getId() {
            return id;
        }

        public int getQuantity() {
            return quantity;
        }
        @JsonProperty
        public void setType(String type) {
            this.type = type;
        }
        @JsonProperty("product")
        public void setId(String id) {
            this.id = id;
        }
        @JsonProperty
        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "type='" + type + '\'' +
                    ", id='" + id + '\'' +
                    ", quantity=" + quantity +
                    '}';
        }
    }

    public static class Action {
        ActionType type;
        ActionStatus status;
        double amount;
        String currency;
        Date timeCreated;
        Date timeUpdated;

        public ActionType getType() {
            return type;
        }

        public ActionStatus getStatus() {
            return status;
        }

        public double getAmount() {
            return amount;
        }

        public String getCurrency() {
            return currency;
        }

        public Date getTimeCreated() {
            return timeCreated;
        }

        public Date getTimeUpdated() {
            return timeUpdated;
        }
        @JsonProperty
        public void setType(ActionType type) {
            this.type = type;
        }
        @JsonProperty
        public void setStatus(ActionStatus status) {
            this.status = status;
        }
        @JsonProperty
        public void setAmount(double amount) {
            this.amount = amount;
        }
        @JsonProperty
        public void setCurrency(String currency) {
            this.currency = currency;
        }
        @JsonProperty("time_created")
        public void setTimeCreated(Date timeCreated) {
            this.timeCreated = timeCreated;
        }
        @JsonProperty("time_updated")
        public void setTimeUpdated(Date timeUpdated) {
            this.timeUpdated = timeUpdated;
        }

        @Override
        public String toString() {
            return "Action{" +
                    "type='" + type + '\'' +
                    ", status='" + status + '\'' +
                    ", amount=" + amount +
                    ", currency='" + currency + '\'' +
                    ", timeCreated='" + timeCreated + '\'' +
                    ", timeUpdated='" + timeUpdated + '\'' +
                    '}';
        }
    }
    
    public static class User {
        String id;
        String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
        @JsonProperty
        public void setId(String id) {
            this.id = id;
        }
        @JsonProperty
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    public static class Application {
        String name;
        String id;
        String namespace;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getNamespace() {
            return namespace;
        }
        @JsonProperty
        public void setName(String name) {
            this.name = name;
        }
        @JsonProperty
        public void setId(String id) {
            this.id = id;
        }
        @JsonProperty
        public void setNamespace(String namespace) {
            this.namespace = namespace;
        }


        @Override
        public String toString() {
            return "Application{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", namespace='" + namespace + '\'' +
                    '}';
        }
    }

    public static void main(String[] args) throws Exception {
        final String json = "{\"id\":\"301835633280668\",\"user\":{\"name\":\"Anderson S Cunha\",\"id\":\"878550009\"},\"application\":{\"name\":\"Billing Test\",\"namespace\":\"gosu-billing-test\",\"id\":\"121835417932514\"},\"actions\":[{\"type\":\"charge\",\"status\":\"completed\",\"currency\":\"USD\",\"amount\":\"0.30\",\"time_created\":\"2013-06-10T10:07:42+0000\",\"time_updated\":\"2013-06-10T10:07:42+0000\"}],\"refundable_amount\":{\"currency\":\"USD\",\"amount\":\"0.30\"},\"items\":[{\"type\":\"IN_APP_PURCHASE\",\"product\":\"https:\\/\\/s3.amazonaws.com\\/gosu_bill\\/x\\/static\\/products\\/1\",\"quantity\":1}],\"country\":\"US\",\"created_time\":\"2013-06-10T10:07:42+0000\",\"test\":1,\"fraud_status\":\"UNKNOWN\",\"payout_foreign_exchange_rate\":1}";
        ObjectReader reader = JsonUtils.JSON_MAPPER.reader(Payment.class);
        Payment pay = reader.readValue(json);
        System.out.println(pay);

    }


}
