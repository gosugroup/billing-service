package services.networks.facebook.graph.payment;


import com.fasterxml.jackson.annotation.JsonCreator;

public enum DisputeReason {
    _UNRECOGNIZED_ ("for any other unsupported value"),
    PENDING ("pending"),
    REFUNDED_IN_CASH ("refunded_in_cash"),
    GRANTED_REPLACEMENT_ITEM ("granted_replacement_item"),
    DENIED_REFUND ("denied_refund"),
    BANNED_USER ("banned_user");
    ;
    private String value;

    private DisputeReason(String value) {
        this.value = value;
    }

    @JsonCreator
    public static DisputeReason fromValue(String v) {
        for (DisputeReason myEnum : values()) {
            if (myEnum.value.equals(v)) {
                return myEnum;
            }
        }
        return _UNRECOGNIZED_;
    }
}
