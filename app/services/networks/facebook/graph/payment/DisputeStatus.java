package services.networks.facebook.graph.payment;


import com.fasterxml.jackson.annotation.JsonCreator;

public enum DisputeStatus {
    _UNRECOGNIZED_ ("for any other unsupported value"),
    PENDING ("pending"),
    RESOLVED ("resolved"),

    ;

    private String value;

    private DisputeStatus(String value) {
        this.value = value;
    }

    @JsonCreator
    public static DisputeStatus fromValue(String v) {
        for (DisputeStatus myEnum : values()) {
            if (myEnum.value.equals(v)) {
                return myEnum;
            }
        }
        return _UNRECOGNIZED_;
    }
}
