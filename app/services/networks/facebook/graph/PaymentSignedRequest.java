package services.networks.facebook.graph;

import com.fasterxml.jackson.annotation.JsonProperty;
import services.exception.BillingException;
import services.networks.facebook.graph.payment.ActionStatus;

/**
 * Created by shuping on 4/9/15.

 Sample:

 {
 "algorithm":"HMAC-SHA256",
 "amount":"5.00",
 "currency":"USD",
 "issued_at":1387285426,
 "payment_id":495869157196092,
 "quantity":"1",
 "request_id":"60046727",
 "status":"completed"
 }

 */
public class PaymentSignedRequest {

    @JsonProperty
    private String algorithm;

    @JsonProperty
    private double amount;

    @JsonProperty
    private String currency;

    @JsonProperty(value = "issued_at")
    private long issuedAt;

    @JsonProperty(value = "payment_id")
    private String paymentId;

    @JsonProperty
    private int quantity;

    /**
     * We use request_id to carry additional info and make it unique in this way:
     * request_id = <user_id>_<product_id>_<timestamp>
     */
    @JsonProperty(value = "request_id")
    private String requestId;

    @JsonProperty
    private ActionStatus status;


    public String getUserId() throws BillingException {
        checkRequestId();
        return requestId.split("_")[0];
    }
    public String getProductId() throws BillingException {
        checkRequestId();
        return requestId.split("_")[1];
    }

    private void checkRequestId() throws BillingException {
        if (requestId == null) {
            throw new BillingException("requestId not set hence can't parse userId from it.");
        }
        String[] splits = requestId.split("_");
        if (splits.length != 3) {
            throw new BillingException("requestId not in format of <user_id>_<product_id>_<timestamp>: " + requestId);
        }
    }


    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(long issuedAt) {
        this.issuedAt = issuedAt;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public ActionStatus getStatus() {
        return status;
    }

    public void setStatus(ActionStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PaymentSignedRequest{" +
                "algorithm='" + algorithm + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", issuedAt=" + issuedAt +
                ", paymentId='" + paymentId + '\'' +
                ", quantity=" + quantity +
                ", requestId='" + requestId + '\'' +
                ", status=" + status +
                '}';
    }
}
