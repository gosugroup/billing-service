package services.networks.facebook.graph;

import com.fasterxml.jackson.databind.ObjectReader;
import play.Logger;
import play.libs.ws.WS;
import services.networks.facebook.FacebookGraphApiException;
import services.networks.facebook.graph.payment.DisputeReason;
import services.networks.facebook.graph.payment.Payment;
import services.utils.JsonUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

/**
 * Created by shuping on 4/17/15.
 */
public class FacebookCall {

    private static final Logger.ALogger LOG = Logger.of(FacebookCall.class);

    public static final ObjectReader paymentReader = JsonUtils.JSON_MAPPER.reader(Payment.class);
    public static final ObjectReader paymentGraphResponseReader = JsonUtils.JSON_MAPPER.reader(PaymentGraphResponse.class);

    private String accessToken;


    public FacebookCall(String accessToken) {
        this.accessToken = accessToken;
    }

    public FacebookCall(String appId, String appSecret) {
        this.accessToken = appId + "|" + appSecret;
    }


    public Payment getFacebookPayment(String transactionId) throws IOException {
        InputStream is = WS.url("https://graph.facebook.com/" + transactionId)
                .setQueryParameter("access_token", accessToken)
                .get()
                .get(10, TimeUnit.SECONDS)
                .getBodyAsStream();

        return paymentReader.readValue(is);
    }

    public void refundFacebookPayment(String paymentId, String currency, double amount) throws IOException, FacebookGraphApiException {
        String body = WS.url("https://graph.facebook.com/" + paymentId + "/refunds")
                .setQueryParameter("access_token", accessToken)
                .post("currency="+currency + "&amount="+amount + "&reason="+"CUSTOMER_SERVICE")
                .get(10, TimeUnit.SECONDS)
                .getBody();
        LOG.debug("refund facebook payment call: {}", body);

        PaymentGraphResponse resp = paymentGraphResponseReader.readValue(body);
        if (resp.getError() != null) {
            throw new FacebookGraphApiException(resp.getError().getMessage());
        }
    }

    public void resolveFacebookDispute(String paymentId, DisputeReason reason) throws IOException, FacebookGraphApiException {
        String body = WS.url("https://graph.facebook.com/" + paymentId + "/dispute")
                .setQueryParameter("access_token", accessToken)
                .post("reason=" + reason.name())
                .get(10, TimeUnit.SECONDS)
                .getBody();
        LOG.debug("resolve facebook payment call: {}", body);

        PaymentGraphResponse resp = paymentGraphResponseReader.readValue(body);
        if (resp.getError() != null) {
            throw new FacebookGraphApiException(resp.getError().getMessage());
        }
    }



}
