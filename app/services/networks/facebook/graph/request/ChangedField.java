package services.networks.facebook.graph.request;


import com.fasterxml.jackson.annotation.JsonCreator;

public enum ChangedField {

    ACTIONS ("actions"),
    DISPUTES("disputes")
    ;
    private String value;

    private ChangedField(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ChangedField fromValue(String v) {
        for (ChangedField myEnum : values()) {
            if (myEnum.value.equals(v)) {
                return myEnum;
            }
        }
        throw new IllegalArgumentException("invalid string value passed: " + v);
    }
}
