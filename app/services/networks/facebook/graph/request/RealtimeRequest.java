package services.networks.facebook.graph.request;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RealtimeRequest {

    @JsonProperty
    private String object;
    @JsonProperty("entry")
    private List<Entry> entries;

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    @Override
    public String toString() {
        return "RealtimeRequest{" +
                "object='" + object + '\'' +
                ", entries=" + entries +
                '}';
    }

    public static class Entry {
        @JsonProperty
        private long id;
        @JsonProperty
        private long time;
        @JsonProperty("changed_fields")
        private List<ChangedField> changedFields;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public List<ChangedField> getChangedChangedFields() {
            return changedFields;
        }

        public void setChangedChangedFields(List<ChangedField> changedChangedFields) {
            this.changedFields = changedChangedFields;
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "id=" + id +
                    ", time=" + time +
                    ", changedFields=" + changedFields +
                    '}';
        }
    }

    public static void main(String args[]) throws Exception {
        final String str = "{\"object\":\"payments\",\"entry\":[{\"id\":\"301835666613998\",\"time\":1370863076,\"changed_fields\":[\"actions\"]}]}";
        ObjectMapper mapper = new ObjectMapper();
        RealtimeRequest req = mapper.readValue(str, RealtimeRequest.class);
        System.out.println(req);
    }
}
