package services.networks.facebook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author shuping
 */
public class FacebookBillingNotifier {
    
    private static String readStringAndClose(InputStream is) throws IOException {
		if(null == is) {
			return null;
		}
		Writer writer = new StringWriter();
		char[] buffer = new char[1024];
		try {
			Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int n;
			while ((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}
		} finally {
			is.close();
		}
		return writer.toString();
	}

    private static String calculateBillingReqeustSignature(byte[] payload, String secret) {
        try {
            SecretKeySpec key = new SecretKeySpec((secret).getBytes("UTF-8"), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(key);
            
            byte[] bytes = mac.doFinal(payload);
            
            String calculated = Hex.encodeHexString(bytes);
            System.out.println("Calculated signature: " + calculated);
            
            return calculated;
            
        } catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public static void main(String[] args) throws MalformedURLException, IOException {
        String URL_BHF_LIVE = "http://54.148.93.35:8080/g/fbbill/"; //https://combatelite.gosugroup.net/g/fbbill/";
        String SECRET_BHF_LIVE = "7ef6d5eb256eb5445a35e9b3910bdc57";
        
        //String URL_BHF_QA = "https://dev2.gosugroup.com/g/fbbill/";
        //String SECRET_BHF_QA   = "3b0d0ec64b6f77ac658fcbbf4a4cee32";
        
        String message = 
            "{\n" +
            "  \"object\": \"payments\",\n" +
            "  \"entry\": [\n" +
            "    {\n" +
            "      \"id\": \"%d\",\n" +
            "      \"time\": %d,\n" +
            "      \"changed_fields\": [\n" +
            "        \"actions\"\n" +
            "      ]\n" +
            "    }\n" +
            "  ]\n" +
            "}";
        
        message = String.format(message, 637652823011444L, System.currentTimeMillis());
        
        String signature;
        signature = calculateBillingReqeustSignature(message.getBytes(), SECRET_BHF_LIVE);

        URL url  = new URL(URL_BHF_LIVE);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setDoInput (true);
		conn.setDoOutput (true);
		conn.setRequestMethod ("POST");

        conn.setRequestProperty("X-Hub-Signature", "sha1="+signature);
        conn.getOutputStream().write(message.getBytes());

        int status = conn.getResponseCode();
		System.out.println("http request status: " + status);		

        InputStream in = (status == HttpURLConnection.HTTP_OK) ? 
            conn.getInputStream() : conn.getErrorStream();
        
        String result = readStringAndClose(in);
        System.out.println(result);
        
	}
	

}
