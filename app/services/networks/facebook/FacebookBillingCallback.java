package services.networks.facebook;

import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import models.Game;
import org.apache.commons.codec.binary.Hex;
import play.Logger;
import services.utils.JsonUtils;
import services.networks.facebook.graph.request.RealtimeRequest;

/**
 * GOSU GROUP
 * User: acunha
 * Date: 03/06/13
 * Time: 17:11
 */
public class FacebookBillingCallback {

    private static final Logger.ALogger LOG = Logger.of(FacebookBillingCallback.class);
    private static final ObjectReader jsonReader = JsonUtils.JSON_MAPPER.reader(RealtimeRequest.class);


    public String subscriptionVerification(Game game, String mode, String verifyToken, String challenge) {
        if ("subscribe".equals(mode) && game.getApiToken().equals(verifyToken)) {
            return challenge;
        }
        return null;
    }

    public boolean paymentUpdate(Game game, byte[] bodyBytes, String xHubSignatureHeader) {
        LOG.debug("Billing request: {}", new String(bodyBytes));

        try {
            validateReqeust(bodyBytes, xHubSignatureHeader, game.getFbAppSecret());
            processPayments(game, bodyBytes);
            return true;

        } catch (IOException | FacebookGraphApiException ex) {
            LOG.error("Failed to process realtime update:", ex);
            return false;
        }
    }

    private void processPayments(Game game, byte[] data) throws IOException {

        final RealtimeRequest req = jsonReader.readValue(data);
        for (RealtimeRequest.Entry entry : req.getEntries()) {
            LOG.debug("Billing request decoded entry: {}", entry);

            final FacebookBillingClient client = new FacebookBillingClient(game);
            client.processPaymentUpdate(String.valueOf((entry.getId())));
        }
    }

    private void validateReqeust(byte[] payload, String sigHeader, String secret) throws FacebookGraphApiException {
        try {
            LOG.debug("validating request with provided sigHeader: {}, secret: {}", sigHeader, secret);
            if (sigHeader == null) {
                throw new FacebookGraphApiException("missing sigHeader, must be wrong facebook request.");
            }
            if (secret == null) {
                throw new FacebookGraphApiException("Game's facebook secret not configured");
            }

            SecretKeySpec key = new SecretKeySpec((secret).getBytes("UTF-8"), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(key);
            
            byte[] bytes = mac.doFinal(payload);
            
            String calculated = Hex.encodeHexString(bytes);
            calculated = "sha1=" + calculated;
            LOG.debug("Calculated signature: {}", calculated);
            
            if(! sigHeader.equals(calculated)) {    
                throw new FacebookGraphApiException("Signature validation faild. The request might be fake.");
            }
        } catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

}