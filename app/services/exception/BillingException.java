package services.exception;


public class BillingException extends Exception {

    public BillingException(Throwable e) {
        super(e);
    }

    public BillingException(String msg) {
        super(msg);
    }
}
