package services.exception;

/**
 * Created by acunha on 1/16/14.
 */
public class DecodeTransactionException extends Exception {
    public DecodeTransactionException() {
        super();
    }

    public DecodeTransactionException(Throwable cause) {
        super(cause);
    }
}
