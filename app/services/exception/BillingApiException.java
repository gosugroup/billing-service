package services.exception;

/**
 * Created by shuping on 4/15/15.
 */
public class BillingApiException extends RuntimeException {


    public BillingApiException() {
        super();
    }

    public BillingApiException(String message) {
        super(message);
    }

    public BillingApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public BillingApiException(Throwable cause) {
        super(cause);
    }

    protected BillingApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
