package services.exception;

/**
 * Created by acunha on 1/14/14.
 */
public class InvalidTransactionException extends Exception {

    public InvalidTransactionException() {
        super();
    }

    public InvalidTransactionException(String message) {
        super(message);
    }
}
