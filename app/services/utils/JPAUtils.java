package services.utils;

import play.db.jpa.JPA;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * Created by shuping on 4/15/15.
 */
public class JPAUtils {

    public static <T> List<T> findAll(Class<T> klass) {
        CriteriaBuilder cb = JPA.em().getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(klass);

        return JPA.em().createQuery(cq.select(cq.from(klass))).getResultList();
    }

    public static int deleteAll(Class<?> klass) {
        CriteriaBuilder cb = JPA.em().getCriteriaBuilder();
        CriteriaDelete cd = cb.createCriteriaDelete(klass);
        cd.from(klass);

        return JPA.em().createQuery(cd).executeUpdate();
    }



}
