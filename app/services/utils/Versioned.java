package services.utils;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Created by shuping on 4/7/15.
 */
@MappedSuperclass
public class Versioned {

    @Version
    protected int _version;
}
