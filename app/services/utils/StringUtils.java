package services.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class StringUtils {

    private static final String ENCODING = "UTF-8";

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String urlDecode(String s) {
        try {
            return URLDecoder.decode(s, ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] base64Decode(String s) {
        try {
            return Base64.decodeBase64(s.getBytes(ENCODING));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] urlAndBase64Decode(String s) {
        return base64Decode(urlDecode(s));
    }

    public static String hexEncode(byte[] b) {
        try {
            return new String(new Hex().encode(b), ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String underscoreToCamelCase(String input) {
        StringBuilder sb = new StringBuilder();
        char[] array = input.toCharArray();
        for(int i=0; i<array.length; i++) {
            if (array[i] == '_' && i<array.length-1 && 'a' <= array[i+1] && array[i+1] <= 'z') {
                sb.append(Character.toUpperCase(array[i+1]));
                i++;    //skip one more char
            } else {
                sb.append(array[i]);
            }
        }
        return sb.toString();
    }

    public static boolean isEmpty(String s) {
        return s==null || s.isEmpty();
    }
}

