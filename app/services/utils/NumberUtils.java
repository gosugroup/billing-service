package services.utils;

/**
 * Created by shuping on 4/10/15.
 */
public class NumberUtils {

    public static boolean equals(double d1, double d2) {
        return (Math.abs(d1 - d2) < 0.0001);
    }

}
