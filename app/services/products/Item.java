package services.products;


import services.products.xmlConfig.ItemConfig;

public class Item {
    private final int typeId;
    private final int repeatQuantity;
    private final int purchaseQuantity;
    private int quantity;

    public Item(ItemConfig config) {
        this.typeId = config.getId();
        this.quantity = config.getQuantity();
        this.repeatQuantity = config.getRepeatQuantity();
        this.purchaseQuantity = config.getPurchaseAmount();
    }
    
    public Item(int typeId,int quantity) {
        this.typeId = typeId;
        this.quantity = quantity;
        this.purchaseQuantity = quantity;
        this.repeatQuantity = quantity;
    }

    public int getTypeId() {
        return typeId;
    }

    public int getRepeatQuantity() {
        return repeatQuantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPurchaseQuantity() {
        return purchaseQuantity;
    }

    public void multiplyQuantity(int coefficient) {
        this.quantity*=coefficient;
    }
}
