package services.products;

import services.products.xmlConfig.ItemConfig;
import services.products.xmlConfig.ProductConfig;

import java.util.ArrayList;
import java.util.List;

public class Product {

    private final String id;
    private final List<Item> items;
    private final List<ProductConfig.Price> price;
	private final int purchaseLimit;

    public Product(ProductConfig config) {
        this.id = config.getId();
        items = new ArrayList<>();
        for (ItemConfig ic : config.getItems()) {
            items.add(new Item(ic));
        }
        if (config.getPrices() != null) {
            price = config.getPrices();
        } else {
            price = new ArrayList<>();
        }
	    purchaseLimit = config.getPurchaseLimit();
    }

    public String getId() {
        return id;
    }

    public List<Item> getItems() {
        return items;
    }

    public double getPrice(String currency) {
        for (ProductConfig.Price p: price) {
            if (p.getCurrency().equals(currency)) {
                return p.getAmount();
            }
        }
        return 0;
    }

	public int getPurchaseLimit() {
		return purchaseLimit;
	}
}
