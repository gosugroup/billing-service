package services.products.xmlConfig;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Item")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemConfig {
    @XmlAttribute
    private int id;
    @XmlAttribute
    private int quantity;
    @XmlAttribute
    private int repeatQuantity;
    @XmlAttribute
    private int purchaseAmount;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    

	public int getRepeatQuantity() {
		return repeatQuantity;
	}

	public void setRepeatQuantity(int repeatQuantity) {
		this.repeatQuantity = repeatQuantity;
	}

    public int getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(int purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    @Override
    public String toString() {
        return "Item{"+id +"=" + quantity +'}';
    }
}
