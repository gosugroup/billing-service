package services.products.xmlConfig;

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Products")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductConfigList {
    @XmlAttribute(required = true)
    int version;
    @XmlElement(name = "Product", required = true)
    List<ProductConfig> products;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<ProductConfig> getProducts() {
        return products;
    }

    public void setProducts(List<ProductConfig> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "FacebookBillingProductList{" +
                "version=" + version +
                ", products=" + products +
                '}';
    }

    public static void main(String[] args) throws Exception {

            JAXBContext jc = JAXBContext.newInstance( ProductConfigList.class );
            Unmarshaller u = jc.createUnmarshaller();
            System.out.println(u.unmarshal(ProductConfigList.class.getResourceAsStream("/assets/products.xml")));
    }
}
