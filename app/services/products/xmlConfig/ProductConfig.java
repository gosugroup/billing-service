package services.products.xmlConfig;


import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FacebookProduct")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductConfig {
    @XmlAttribute(required = true)
    private String id;
    @XmlAttribute
    private int purchaseLimit;
    @XmlElementWrapper(name = "Prices", required = true)
    @XmlElement (name = "Price", required = true)
    private List<Price> prices;
    @XmlElementWrapper(name = "Items", required = true)
    @XmlElement(name = "Item", required = true)
    private List<ItemConfig> items;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public List<ItemConfig> getItems() {
        return items;
    }

    public void setItems(List<ItemConfig> items) {
        this.items = items;
    }

    public int getPurchaseLimit() {
		return purchaseLimit;
	}

	public void setPurchaseLimit(int purchaseLimit) {
		this.purchaseLimit = purchaseLimit;
	}

	@Override
    public String toString() {
        return "FacebookProduct{" +
                "id=" + id +
                ", prices=" + prices +
                ", items=" + items +
                '}';
    }

    @XmlRootElement(name = "Price")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Price {
        @XmlAttribute
        private String currency;
        @XmlAttribute
        private Double amount;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }
        
        

        @Override
        public String toString() {
            return "Price{" +
                    "currency='" + currency + '\'' +
                    ", amount=" + amount +
                    '}';
        }
    }


}