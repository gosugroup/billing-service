package services.products;

import services.products.xmlConfig.ProductConfig;
import services.products.xmlConfig.ProductConfigList;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class ProductFactory {

    private final Map<String, ProductConfig> productMap = new HashMap<>();

    public ProductFactory(String productsXml) {
        try {
            JAXBContext jc = JAXBContext.newInstance( ProductConfigList.class );
            Unmarshaller u = jc.createUnmarshaller();
            final ProductConfigList productList = (ProductConfigList)u.unmarshal(new StringReader(productsXml));
            if (productList.getProducts() != null) {
                for (ProductConfig p : productList.getProducts()) {
                    productMap.put(p.getId(), p);
                }
            }

        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    public Product getProduct(String id) {
        final ProductConfig config = productMap.get(id);
        if (config == null) {
            return null;
        }
        return new Product(config);
    }

    public ProductConfig getProductConfig(String id) {
        return productMap.get(id);
    }
}
