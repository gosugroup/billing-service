package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.BillingApi;
import play.Logger;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.BillingResponse;
import services.networks.apple.AppleBillingClient;


public class AppleBillingApi extends BillingApi {

    private static Logger.ALogger LOG = Logger.of(AppleBillingApi.class);


    @BodyParser.Of(value=BodyParser.Json.class)
    public static Result handlePayment(String gameId) {
        JsonNode json = request().body().asJson();

        String uid = readJsonKey(json, "user_id", JsonNode::asText);
        String transactionId = readJsonKey(json, "transaction_id", JsonNode::asText);
        String clientReceipt = readJsonKey(json, "client_receipt", JsonNode::asText);
        boolean markDelivered = readJsonKey(json, "mark_delivered", JsonNode::asBoolean, true);

        AppleBillingClient client = new AppleBillingClient(loadGameAndValidateApiToken(gameId));
        BillingResponse resp = client.handlePayment(uid, transactionId, clientReceipt, markDelivered);
        return ok(resp.toJson());
    }

    @BodyParser.Of(value=BodyParser.Json.class)
    public static Result markDelivery(String gameId) {
        JsonNode json = request().body().asJson();

        String orderId = readJsonKey(json, "order_id", JsonNode::asText);
        boolean markDelivered = readJsonKey(json, "mark_delivered", JsonNode::asBoolean, true);

        AppleBillingClient client = new AppleBillingClient(loadGameAndValidateApiToken(gameId));
        BillingResponse resp = client.markDelivery(orderId, markDelivered);
        return ok(resp.toJson());
    }

}
