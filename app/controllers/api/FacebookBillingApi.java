package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.BillingApi;
import models.Game;
import play.Logger;
import play.libs.F;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.BillingResponse;
import services.dao.GameDao;
import services.networks.facebook.FacebookBillingCallback;
import services.networks.facebook.FacebookBillingClient;

import java.util.concurrent.TimeUnit;

public class FacebookBillingApi extends BillingApi {

    private static Logger.ALogger LOG = Logger.of(FacebookBillingApi.class);


    @BodyParser.Of(value=BodyParser.Empty.class)
    public static Result subscriptionVerificationCallback(String gameId) {
        Game game = new GameDao().findWithCache(gameId);

        String mode = request().getQueryString("hub.mode");
        String verifyToken = request().getQueryString("hub.verify_token");
        String challenge = request().getQueryString("hub.challenge");

        LOG.debug("facebook subscription verification request: mode={}, verify_token={}, challenge={}",
                mode, verifyToken, challenge);
        String answer = new FacebookBillingCallback().subscriptionVerification(game, mode, verifyToken, challenge);
        return answer != null ? ok(answer) : badRequest();
    }

    @BodyParser.Of(value=BodyParser.Raw.class)
    public static F.Promise<Result> paymentUpdateCallback(String gameId) {
        Game game = new GameDao().findWithCache(gameId);

        final byte[] bodyBytes = request().body().asRaw().asBytes();
        String xHubSignature = request().getHeader("X-Hub-Signature");
        LOG.debug("facebook callback: {} bytes, signature; {}", bodyBytes.length, xHubSignature);

        // Delay all facebook realtime update callbacks for 10 seconds.
        return F.Promise.delayed(()-> {
            boolean result = new FacebookBillingCallback().paymentUpdate(game, bodyBytes, xHubSignature);
            return result ? ok() : badRequest();
        }, 10, TimeUnit.SECONDS);
    }

    @BodyParser.Of(value=BodyParser.Json.class)
    public static Result handlePaymentRequest(String gameId) {
        JsonNode json = request().body().asJson();
        LOG.debug("Facebook handle payment request body: {}", json.toString());

        String signedRequest = readJsonKey(json, "signed_request", JsonNode::asText);
        boolean markDelivered = readJsonKey(json, "mark_delivered", JsonNode::asBoolean, true);

        FacebookBillingClient client = new FacebookBillingClient(loadGameAndValidateApiToken(gameId));
        BillingResponse resp = client.processSignedRequest(signedRequest, markDelivered);
        return ok(resp.toJson());
    }

    @BodyParser.Of(value=BodyParser.Json.class)
    public static Result markDelivery(String gameId) {
        JsonNode json = request().body().asJson();
        LOG.debug("Facebook mark delivery request body: {}", json.toString());

        String orderId = readJsonKey(json, "order_id", JsonNode::asText);
        boolean markDelivered = readJsonKey(json, "mark_delivered", JsonNode::asBoolean, true);

        FacebookBillingClient client = new FacebookBillingClient(loadGameAndValidateApiToken(gameId));
        BillingResponse resp = client.markDelivery(orderId, markDelivered);
        return ok(resp.toJson());
    }


    @BodyParser.Of(value=BodyParser.Json.class)
    public static Result resolvePaymentDispute(String gameId) {
        JsonNode json = request().body().asJson();
        LOG.debug("Facebook resolve dispute request body: {}", json.toString());

        String orderId = readJsonKey(json, "order_id", JsonNode::asText);
        String refundCurrency = readJsonKey(json, "refund_currency", JsonNode::asText);
        double refundAmount = readJsonKey(json, "refund_amount", JsonNode::asDouble);
        String emailTitle = readJsonKey(json, "email_subject", JsonNode::asText, null);
        String emailBody = readJsonKey(json, "email_body", JsonNode::asText, null);

        FacebookBillingClient client = new FacebookBillingClient(loadGameAndValidateApiToken(gameId));
        BillingResponse resp = client.resolvePaymentDispute(orderId, refundCurrency, refundAmount, emailTitle, emailBody);
        return ok(resp.toJson());
    }
}
