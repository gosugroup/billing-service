package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.BillingApi;
import play.Logger;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.BillingResponse;
import services.networks.google.GoogleBillingClient;

public class GoogleBillingApi extends BillingApi {

    private static Logger.ALogger LOG = Logger.of(GoogleBillingApi.class);

    @BodyParser.Of(value=BodyParser.Json.class)
    public static Result handlePayment(String gameId) {
        JsonNode json = request().body().asJson();

        String userId = readJsonKey(json, "user_id", JsonNode::asText);
        String purchaseData = readJsonKey(json, "purchase_data", JsonNode::asText);
        String dataSignature = readJsonKey(json, "data_signature", JsonNode::asText);
        boolean markDelivered = readJsonKey(json, "mark_delivered", JsonNode::asBoolean, true);

        GoogleBillingClient client = new GoogleBillingClient(loadGameAndValidateApiToken(gameId));
        BillingResponse resp = client.handlePayment(userId, purchaseData, dataSignature, markDelivered);
        return ok(resp.toJson());
    }


    @BodyParser.Of(value=BodyParser.Json.class)
    public static Result markDelivery(String gameId) {
        JsonNode json = request().body().asJson();

        String orderId = readJsonKey(json, "order_id", JsonNode::asText);
        boolean markDelivered = readJsonKey(json, "mark_delivered", JsonNode::asBoolean, true);

        GoogleBillingClient client = new GoogleBillingClient(loadGameAndValidateApiToken(gameId));
        BillingResponse resp = client.markDelivery(orderId, markDelivered);
        return ok(resp.toJson());
    }



}
