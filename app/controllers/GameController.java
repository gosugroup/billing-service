package controllers;

import models.Game;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.dao.GameDao;
import views.html.*;

import java.io.*;
import java.util.UUID;

import static play.data.Form.*;

/**
 * Created by shuping on 3/27/15.
 */
@Security.Authenticated(Secured.class)
public class GameController extends Controller {

    private static Logger.ALogger log = Logger.of(GameController.class);

    private static GameDao dao = new GameDao();

    public static Result GAMES_HOME = redirect(routes.GameController.list());

    public static Result list() {
        return ok(gameList.render(dao.listForDashboard()));
    }

    public static Result create() {
        Form<Game> gameForm = form(Game.class);
        return ok(gameCreate.render(dao.list(), gameForm));
    }

    public static Result save() {
        Form<Game> gameForm = form(Game.class).bindFromRequest();
        readProductsXml(gameForm);

        // generate a UUID as api token.
        if (gameForm.get().getApiToken() == null || gameForm.get().getApiToken().isEmpty()) {
            gameForm.get().setApiToken(UUID.randomUUID().toString());
        }

        if (!dao.save(gameForm.get())) {
            gameForm.reject("id", "Duplicated game id or other errors");
        }

        if (gameForm.hasErrors()) {
            return badRequest(gameCreate.render(dao.list(), gameForm));
        }
        flash("success", "Game " + gameForm.get().getId() + " has been created.");
        return GAMES_HOME;
    }

    public static Result edit(String id) {
        Form<Game> gameForm = form(Game.class).fill(dao.find(id));
        return ok(gameEdit.render(dao.list(), id, gameForm));
    }

    public static Result update(String id) {
        Form<Game> gameForm = form(Game.class).bindFromRequest();
        readProductsXml(gameForm);

        if (!dao.update(gameForm.get(), id)) {
            gameForm.reject("id", "Failed to update game.");
        }

        if(gameForm.hasErrors()) {
            return badRequest(gameEdit.render(dao.list(), id, gameForm));
        }
        flash("success", "Game " + id + " has been updated");
        return edit(id);
    }

    public static Result delete(String id) {
        dao.delete(id);
        return GAMES_HOME;
    }


    private static void readProductsXml(Form<Game> gameForm) {
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart xml = body.getFile("productsXmlFile");
        if (xml == null) {
            return; // it's ok if it's not updated
        }

        if (! xml.getContentType().contains("xml")) {
            gameForm.reject("productsXmlFile", "Only accept xml file");
        }
        try {
            File file = xml.getFile();
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();

            String str = new String(data, "UTF-8");
            gameForm.get().setProductsXml(str);

        } catch (IOException e) {
            gameForm.reject("productsXmlFile", "Failed to read content of the file");
        }
    }

}
