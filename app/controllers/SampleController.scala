package controllers

import play.api.mvc._

/**
 * Java API doesn't give you a chance to handle the request before the body has been parsed,
 * if you dispatch to another action after the body has been consumed/parsed, well, that just won't work,
 * since you can't consume the body twice.
 *
 * But there's nothing stopping you from implementing it in Scala (you can still write and use Scala actions
 * in a Play Java project.
 */
object SampleController {

  def serve (platform: String) = EssentialAction { request: RequestHeader =>
      controllers.Assets.at("/public", platform + "_sample.html")(request)
  }
}
