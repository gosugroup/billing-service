package controllers;

import models.Account;
import static play.data.Form.*;

import play.data.Form;
import play.mvc.*;

import views.html.*;

import play.db.jpa.Transactional;

public class Application extends Controller {

    // -- Authentication

    public static class Login {

        public String email;
        public String password;

        public String validate() {
            if(Account.authenticate(email, password) == null) {
                return "Invalid user or password";
            }
            return null;
        }

    }

    /**
     * Login page.
     */
    public static Result login() {
        return ok(login.render(form(Login.class)));
    }

    /**
     * Handle login form submission.
     */
    @Transactional
    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if(loginForm.hasErrors()) {
            return badRequest(login.render(loginForm));
        } else {
            session("email", loginForm.get().email);
            return redirect(routes.GameController.list()
            );
        }
    }

    /**
     * Logout and clean the session.
     */
    public static Result logout() {
        session().clear();
        flash("success", "You've been logged out");
        return redirect(routes.Application.login());
    }

}
