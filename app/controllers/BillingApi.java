package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Game;
import play.Logger;
import play.mvc.Controller;
import services.dao.GameDao;
import services.exception.BillingApiException;
import services.utils.StringUtils;

import java.util.function.Function;

public class BillingApi extends Controller {

    private static Logger.ALogger LOG = Logger.of(BillingApi.class);


    protected static Game loadGameAndValidateApiToken(String gameId) {
        Game game = new GameDao().findWithCache(gameId);
        if (game == null) {
            throw new BillingApiException("Unrecognized game id: " + gameId);
        }

        String apiToken = request().getHeader("api_token");
        if (apiToken == null || !apiToken.equals(game.getApiToken())) {
            throw new BillingApiException("http header api_token not provided or wrong.");
        }

        return game;
    }


    protected static <T> T readJsonKey(JsonNode node, String key, Function<JsonNode, T> func, T defaultValue) {
        return readJsonKey(node, key, func, false, defaultValue);
    }

    protected static <T> T readJsonKey(JsonNode node, String key, Function<JsonNode, T> func) {
        return readJsonKey(node, key, func, true, null);
    }

    private static <T> T readJsonKey(JsonNode node, String key, Function<JsonNode, T> func, boolean required, T defaultValue) {
        JsonNode find = node.get(key);
        String key2 = null;
        if (find == null) {
            key2 = StringUtils.underscoreToCamelCase(key);
            find = node.get(key2);
        }

        if (find == null) {
            if (required) {
                throw new BillingApiException("Json property required but missing: " + key + " or " + key2);
            }
            return defaultValue;
        }
        return func.apply(find);
    }


}
