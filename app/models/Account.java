package models;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import play.data.format.*;
import play.data.validation.*;
import play.db.jpa.JPA;
import services.utils.PasswordHash;


@Entity 
@Table(name="account")
public class Account {

    private static final long serialVersionUID = 1L;

	@Id
    @Constraints.Required
    @Formats.NonEmpty
    public String email;
    
    @Constraints.Required
    public String name;
    
    @Constraints.Required
    public String password;
    

    /**
     * Retrieve an account from email.
     */
    public static Account findByEmail(String email) {
        return JPA.em().find(Account.class, email);
    }

    public static Account findByEmailAndPassword(String email, String password) {
        CriteriaBuilder cb = JPA.em().getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> userRoot = cq.from(Account.class);
        cq.select(userRoot).where(
                cb.equal(userRoot.get("email"), email),
                cb.equal(userRoot.get("password"), password)
        );
        List<Account> results = JPA.em().createQuery(cq).getResultList();
        return results.isEmpty() ? null : results.get(0);
    }

    /**
     * Authenticate an account.
     */
    public static Account authenticate(String email, String password) {

        try {
            Account account = findByEmail(email);
            // we save the secure hash instead of plain password text in db.
            if (account != null && PasswordHash.validatePassword(password, account.password)) {
                return account;
            }
            return null;

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }


    // --
    
    public String toString() {
        return "Account(" + email + ")";
    }

}

