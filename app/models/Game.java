package models;

import com.amazonaws.regions.Regions;
import play.data.validation.Constraints;
import play.db.jpa.JPA;
import services.aws.SESAsyncClient;
import services.aws.SQSSyncClient;
import services.products.ProductFactory;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shuping on 3/26/15.
 */
@Entity
public class Game {

    @Id
    @Constraints.Required
    private String id;

    @Constraints.Required
    private String name;

    private boolean productionMode;

    // The token used for facebook callback verification, and game server to call billing service API.
    private String apiToken;

    private String emailSender;
    private String emailRecipient;

    @Enumerated(EnumType.STRING)
    private Regions awsRegionName;
    private String awsAccessKey;
    private String awsSecretKey;
    private String awsSqsUrl;

    // ios
    private boolean iosTestMode;
    private String iosUpsightApiKey;
    private boolean iosUpsightTestServer;

    // google play
    private String gpAppPublicKey;
    private String gpUpsightApiKey;
    private boolean gpUpsightTestServer;

    // facebook
    private String fbAppId;
    private String fbAppSecret;
    private String fbUpsightApiKey;
    private boolean fbUpsightTestServer;

    private String productsXml;

    @Transient
    private ProductFactory productFactory;
    @Transient
    private SQSSyncClient sqsClient;
    @Transient
    private SESAsyncClient sesClient;
    @Transient
    private Summary summary;

    // for bean initiation.
    public Game() {}

    // helper method for drop down list on view.
    public static List<String> getAvailableRegions() {
        return Arrays.asList(Regions.values()).stream().map(r -> r.name()).collect(Collectors.toList());
    }


    public ProductFactory getProductFactory() {
        if (productFactory == null) {
            productFactory = new ProductFactory(productsXml);
        }
        return productFactory;
    }

    public SQSSyncClient getSQSSyncClient() {
        if (sqsClient == null) {
            sqsClient = new SQSSyncClient(awsRegionName, awsAccessKey, awsSecretKey, awsSqsUrl);
        }
        return sqsClient;
    }

    public SESAsyncClient getSESAsyncClient() {
        if (sesClient == null) {
            sesClient = new SESAsyncClient(awsRegionName, awsAccessKey, awsSecretKey);
        }
        return sesClient;
    }


    public static Game findById(String id) {
        return JPA.em().find(Game.class, id);
    }

    public void update(String id) {
        this.id = id;
        JPA.em().merge(this);
    }

    public void delete() {
        JPA.em().remove(this);
    }

    public void save() {
        JPA.em().persist(this);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailSender() {
        return emailSender;
    }

    public void setEmailSender(String emailSender) {
        this.emailSender = emailSender;
    }

    public String getEmailRecipient() {
        return emailRecipient;
    }

    public void setEmailRecipient(String emailRecipient) {
        this.emailRecipient = emailRecipient;
    }

    public boolean isIosTestMode() {
        return iosTestMode;
    }

    public void setIosTestMode(boolean iosTestMode) {
        this.iosTestMode = iosTestMode;
    }

    public String getGpAppPublicKey() {
        return gpAppPublicKey;
    }

    public void setGpAppPublicKey(String gpAppPublicKey) {
        this.gpAppPublicKey = gpAppPublicKey;
    }

    public String getFbAppSecret() {
        return fbAppSecret;
    }

    public void setFbAppSecret(String fbAppSecret) {
        this.fbAppSecret = fbAppSecret;
    }

    public String getProductsXml() {
        return productsXml;
    }

    public void setProductsXml(String productsXml) {
        this.productsXml = productsXml;
    }

    public String getIosUpsightApiKey() {
        return iosUpsightApiKey;
    }

    public void setIosUpsightApiKey(String iosUpsightApiKey) {
        this.iosUpsightApiKey = iosUpsightApiKey;
    }

    public String getGpUpsightApiKey() {
        return gpUpsightApiKey;
    }

    public void setGpUpsightApiKey(String gpUpsightApiKey) {
        this.gpUpsightApiKey = gpUpsightApiKey;
    }

    public String getFbUpsightApiKey() {
        return fbUpsightApiKey;
    }

    public void setFbUpsightApiKey(String fbUpsightApiKey) {
        this.fbUpsightApiKey = fbUpsightApiKey;
    }

    public String getFbAppId() {
        return fbAppId;
    }

    public void setFbAppId(String fbAppId) {
        this.fbAppId = fbAppId;
    }

    public String getAwsAccessKey() {
        return awsAccessKey;
    }

    public void setAwsAccessKey(String awsAccessKey) {
        this.awsAccessKey = awsAccessKey;
    }

    public String getAwsSecretKey() {
        return awsSecretKey;
    }

    public void setAwsSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }

    public String getAwsSqsUrl() {
        return awsSqsUrl;
    }

    public void setAwsSqsUrl(String awsSqsUrl) {
        this.awsSqsUrl = awsSqsUrl;
    }

    public Regions getAwsRegionName() {
        return awsRegionName;
    }

    public void setAwsRegionName(Regions awsRegionName) {
        this.awsRegionName = awsRegionName;
    }

    public boolean isIosUpsightTestServer() {
        return iosUpsightTestServer;
    }

    public void setIosUpsightTestServer(boolean iosUpsightTestServer) {
        this.iosUpsightTestServer = iosUpsightTestServer;
    }

    public boolean isGpUpsightTestServer() {
        return gpUpsightTestServer;
    }

    public void setGpUpsightTestServer(boolean gpUpsightTestServer) {
        this.gpUpsightTestServer = gpUpsightTestServer;
    }

    public boolean isFbUpsightTestServer() {
        return fbUpsightTestServer;
    }

    public void setFbUpsightTestServer(boolean fbUpsightTestServer) {
        this.fbUpsightTestServer = fbUpsightTestServer;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public boolean isProductionMode() {
        return productionMode;
    }

    public void setProductionMode(boolean productionMode) {
        this.productionMode = productionMode;
    }
}
