package models;


/**
 * Created by shuping on 4/16/15.
 */
public class Summary {

    private int totalFbTransactions;
    private int pendingFbTransactions;

    private int totalIosTransactions;
    private int pendingIosTransactions;

    private int totalGpTransactions;
    private int pendingGpTransactions;

    private int numVisibleMessages;
    private int numInvisibleMessages;


    public int getTotalFbTransactions() {
        return totalFbTransactions;
    }

    public void setTotalFbTransactions(int totalFbTransactions) {
        this.totalFbTransactions = totalFbTransactions;
    }

    public int getPendingFbTransactions() {
        return pendingFbTransactions;
    }

    public void setPendingFbTransactions(int pendingFbTransactions) {
        this.pendingFbTransactions = pendingFbTransactions;
    }

    public int getTotalIosTransactions() {
        return totalIosTransactions;
    }

    public void setTotalIosTransactions(int totalIosTransactions) {
        this.totalIosTransactions = totalIosTransactions;
    }

    public int getPendingIosTransactions() {
        return pendingIosTransactions;
    }

    public void setPendingIosTransactions(int pendingIosTransactions) {
        this.pendingIosTransactions = pendingIosTransactions;
    }

    public int getTotalGpTransactions() {
        return totalGpTransactions;
    }

    public void setTotalGpTransactions(int totalGpTransactions) {
        this.totalGpTransactions = totalGpTransactions;
    }

    public int getPendingGpTransactions() {
        return pendingGpTransactions;
    }

    public void setPendingGpTransactions(int pendingGpTransactions) {
        this.pendingGpTransactions = pendingGpTransactions;
    }

    public void setNumVisibleMessages(int numVisibleMessages) {
        this.numVisibleMessages = numVisibleMessages;
    }

    public int getNumVisibleMessages() {
        return numVisibleMessages;
    }

    public void setNumInvisibleMessages(int numInvisibleMessages) {
        this.numInvisibleMessages = numInvisibleMessages;
    }

    public int getNumInvisibleMessages() {
        return numInvisibleMessages;
    }
}
