name := """billing-service"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  "org.hibernate" % "hibernate-entitymanager" % "4.3.8.Final", // replace by your jpa implementation
  cache,
  javaWs
)

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.18"


credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

resolvers += "Local Maven Repository" at "file:///"+Path.userHome.absolutePath+"/.m2/repository"

resolvers += "Gosu Release" at "https://mvn.gosugroup.net/nexus/content/repositories/releases/"

resolvers += "Gosu Snapshot" at "https://mvn.gosugroup.net/nexus/content/repositories/snapshots/"

// libraryDependencies += "com.gosugroup.server" % "platform-base" % "0.44"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.2.2"

libraryDependencies += "com.fasterxml.jackson.module" % "jackson-module-afterburner" % "2.2.0"

libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.9.29"

// libraryDependencies += "ws.securesocial" %% "securesocial" % "2.1.4"

// http://grokbase.com/t/gg/play-framework/153519k038/sbt-run-hangs-on-file-change
// fork in run := true

sources in doc in Compile := List()