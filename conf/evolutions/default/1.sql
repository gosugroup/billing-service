
# --- !Ups

CREATE TABLE `account` (
  `email` varchar(128) NOT NULL,
  `password` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,

  PRIMARY KEY (`email`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `account` VALUES ('ops@gosugroup.com', '1000:70629f651d726c60e0e058b8301c4f303c136a6e7afd6906:27d48bbe037db2c353246d2d4baab77d32325b72ab1476e1', 'Gosu Ops');


CREATE TABLE `game` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `production_mode` tinyint(1) DEFAULT '0',
  `api_token` varchar(255) DEFAULT NULL,
  `aws_access_key` varchar(128) DEFAULT NULL,
  `aws_secret_key` varchar(128) DEFAULT NULL,
  `aws_sqs_url` varchar(1024) DEFAULT NULL,
  `aws_region_name` varchar(1024) DEFAULT NULL,
  `email_sender` varchar(255) DEFAULT NULL,
  `email_recipient` varchar(255) DEFAULT NULL,

  `fb_app_id` varchar(128) DEFAULT NULL,
  `fb_app_secret` varchar(1024) DEFAULT NULL,
  `fb_upsight_api_key` varchar(1024) DEFAULT NULL,
  `fb_upsight_test_server` tinyint(4) DEFAULT NULL,

  `ios_test_mode` tinyint(1) DEFAULT '0',
  `ios_upsight_api_key` varchar(1024) DEFAULT NULL,
  `ios_upsight_test_server` tinyint(4) DEFAULT NULL,

  `gp_app_public_key` varchar(1024) DEFAULT NULL,
  `gp_upsight_api_key` varchar(1024) DEFAULT NULL,
  `gp_upsight_test_server` tinyint(4) DEFAULT NULL,

  `products_xml` longtext,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `game` VALUES (
    -- id, name
    'billing_dev','Billing Dev','0','12345678-1234-1234-1234-123456789abc',
    -- aws
    'AKIAJS4MQKXLER3656CA','ble6Ui33IlKd9GIwAO1q45I85j3sYqGnOF/ALL3x','https://sqs.ap-southeast-1.amazonaws.com/103304292377/gosu_billing_shuping_dev','SA_EAST_1',
    -- email
    '','',
    -- facebook (using CE FB QA Upsight app)
    '415005038672935','3d29301c421682b625b48e8b63031b96','aeac478b26dd4b1d902817563d8f4816',1,
    -- ios
    1,'',0,
    -- gp
    'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiTBLHoC9TjSjTNqpGv46AqBJWexYZyMDmbzfiL00xaI1jB/b38MFQzV188zEsYwHirDNFvh3W5H4vL2acCQYWqltsK40Y1MMbvVAK+6iweASRTcTlcp/Q0mTkBVAMwNQLkV7fVat4DYJuUoUQhRbFtnnJT87cwF3O+bFer6EraAUZ+ds1HzprcRx8LRjGDuFlxG1W+pMGVOezIoJJM/QcGLSWRLeT5DTPz1boxl49031Mt6CWQnJw2X0WlS4CgAope2+GWOmYDDmWoLGz9sYiaX1TcP0T8JgrkSbWyVbGNvrSh+/w34l5hnfYpqVoUEMQ/fd36LpqVowbG5RV6ewcwIDAQAB','',0,
    -- products
    '<Products>\r\n    <Product id=\"600\">\r\n        <Items>\r\n            <Item id=\"1000\" quantity=\"500\" purchaseAmount=\"500\"/>\r\n        </Items>\r\n        <Prices>\r\n            <Price currency=\"EUR\" amount=\"4\"/>\r\n            <Price currency=\"USD\" amount=\"4.99\"/>\r\n        </Prices>\r\n    </Product>\r\n    <Product id=\"602\">\r\n        <Items>\r\n            <Item id=\"1000\" quantity=\"1200\" purchaseAmount=\"1000\"/>\r\n        </Items>\r\n        <Prices>\r\n            <Price currency=\"EUR\" amount=\"8\"/>\r\n            <Price currency=\"USD\" amount=\"9.99\"/>\r\n        </Prices>\r\n    </Product>\r\n</Products>'
);


CREATE TABLE `facebook_billing_transaction` (
  `order_id` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `game_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `currency` char(3) NOT NULL,
  `status` enum('FAILED','CHARGE','REFUND','DECLINE','CHARGEBACK','CHARGEBACK_REVERSAL','DISPUTE') NOT NULL,
  `exchange_rate` double NOT NULL DEFAULT '1',
  `test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `purchase_event_sent` tinyint(1) NOT NULL DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delivery_pending` tinyint(1) NOT NULL DEFAULT '0',
  `delivery_started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_finished` timestamp,
  `_version` int(11) NOT NULL,

  PRIMARY KEY (`order_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `facebook_billing_dispute` (
  `order_id` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `comment` text,
  `status` varchar(20) NOT NULL,
  `reason` varchar(40) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `refunded_currency` char(3) DEFAULT NULL,
  `refunded_amount` double DEFAULT NULL,
  `_version` int(11) NOT NULL,

  PRIMARY KEY (`order_id`),
  FOREIGN KEY (`order_id`) REFERENCES `facebook_billing_transaction`(`order_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `apple_billing_transaction` (
  `order_id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `game_id` varchar(255) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `test_mode` tinyint(1) DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delivery_pending` tinyint(1) NOT NULL DEFAULT '0',
  `delivery_started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_finished` timestamp,
  `_version` int(11) NOT NULL,

  PRIMARY KEY (`order_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `google_billing_transaction` (
  `order_id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `game_id` varchar(255) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `purchase_state` varchar(9) DEFAULT NULL,
  `developer_payload` varchar(1024) DEFAULT NULL,
  `purchase_token` varchar(1024) DEFAULT NULL,
  `test_mode` tinyint(1) DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delivery_pending` tinyint(1) NOT NULL DEFAULT '0',
  `delivery_started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_finished` timestamp,
  `_version` int(11) NOT NULL,

  PRIMARY KEY (`order_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table account;
drop table game;
drop table facebook_billing_dispute;
drop table facebook_billing_transaction;
drop table apple_billing_transaction;
drop table google_billing_transaction;

SET FOREIGN_KEY_CHECKS=1;

